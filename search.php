<?php 
/**
* search.php
*
* The template for displaying search results.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>

 <?php get_header(); ?>
<!-- main contain area -->
	<div class="container">
		<div class="row">


 <div class="main-content col-md-8" role='main'>
 	
 	<?php if( have_posts()) : ?>

 		<header class="page-header">
	<h1>
	<?php 
printf( __( 'Search Results For %s', 'mi-framework'), get_search_query());
	 ?>
	</h1>

 </header>

<?php while( have_posts()) : the_post(); ?>
<?php get_template_part( 'content', get_post_format() ); ?>
<?php endwhile; ?>
<?php mi_paging_nav(); ?>
<?php else : ?>
	<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>
 </div><!-- end main-content -->

 <?php get_sidebar(); ?>

 <?php get_footer(); ?>