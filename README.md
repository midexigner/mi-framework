MI Framework
==================================

# A list of files that need to be written
* functions.php
* index.php
* header.php
* footer.php
* page.php
* single.php
* content.php (with all content types) after create the content all page, import the 
  https://codex.wordpress.org/Theme_Unit_Test
* author archives
* date archives
* category and tag archives
* 404.php and search.php
* full width template
* contact template
* widgets
* css
* slides
* theme options

Dashboard ( Welcome, System Status, Manual Support)
Pre-built Websites
Manual & Support

- wp_enqueue_script('jquery-ui-core');
- wp_enqueue_script('jquery-ui-widget');
- wp_enqueue_script('jquery-ui-accordion');
- wp_enqueue_script('jquery-ui-autocomplete');
- wp_enqueue_script('jquery-ui-button');
- wp_enqueue_script('jquery-ui-datepicker');
- wp_enqueue_script('jquery-ui-dialog');
- wp_enqueue_script('jquery-ui-draggable');
- wp_enqueue_script('jquery-ui-droppable');
- wp_enqueue_script('jquery-ui-menu');
- wp_enqueue_script('jquery-ui-mouse');
- wp_enqueue_script('jquery-ui-position');
- wp_enqueue_script('jquery-ui-progressbar');
- wp_enqueue_script('jquery-ui-selectable');
- wp_enqueue_script('jquery-ui-resizable');
- wp_enqueue_script('jquery-ui-sortable');
- wp_enqueue_script('jquery-ui-sortable');
- wp_enqueue_script('jquery-ui-slider');
- wp_enqueue_script('jquery-ui-spinner');
- wp_enqueue_script('jquery-ui-tooltip');
- wp_enqueue_script('jquery-ui-tabs');
- wp_enqueue_script('jquery-effects-core');
- wp_enqueue_script('jquery-effects-blind');
- wp_enqueue_script('jquery-effects-bounce');
- wp_enqueue_script('jquery-effects-clip');
- wp_enqueue_script('jquery-effects-drop');
- wp_enqueue_script('jquery-effects-explode');
- wp_enqueue_script('jquery-effects-fade');
- wp_enqueue_script('jquery-effects-fold');
- wp_enqueue_script('jquery-effects-highlight');
- wp_enqueue_script('jquery-effects-pulsate');
- wp_enqueue_script('jquery-effects-scale');
- wp_enqueue_script('jquery-effects-shake');
- wp_enqueue_script('jquery-effects-slide');
- wp_enqueue_script('jquery-effects-transfer');



# Categories Theme Name
- Business
- Entertainment
- Creative
- Blog
- Portfolio
- One Page
- Shop
- Other