<?php 
/**
* 404.php
*
* The template for displaying 404 pages (Not Found).
* Package mi-framework
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>

 <?php get_header(); ?>


<div class="container-404">
<h1><?php _e( 'Error 404 - Nothing Found', 'mi-framework'); ?></h1>
<p><?php _e( 'It looking Like nothing was found here. Maybe try a search?','mi-framework'); ?></p>

<?php get_search_form(); ?>
</div><!-- end container-404 -->


 <?php get_footer(); ?>