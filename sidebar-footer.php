<?php 
/**
* sidebar-footer.php
*
* The primary sidebar.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
*/

 ?>

 <?php if(is_active_sidebar('sidebar-2')):  ?>

 	<aside class="footersidebar" role="complementary">
 	<div class="row">
 	<?php dynamic_sidebar('sidebar-2'); ?>
</div>
 	</aside>
 	<!-- end of sidebar -->
 	<?php endif; ?>