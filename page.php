<?php 
/**
* page.php
*
* The template for displaying all pages.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>
 <?php get_header();  ?>
<!-- main contain area -->
	<div class="container">
		<div class="row">
<!-- content pages area -->
<div class="main-content col-md-8" rol="main">

<?php while(have_posts() ):the_post(); ?>
	
	<article id="post-<?php the_ID();?>" <?php post_class(); ?>>


<!-- Article header -->

<header class="entry-header"><?php

// if the post has a thumbnail and it's not password protected
// then display the thumbnail

if(has_post_thumbnail() && ! post_password_required()):?>

<figure class="entry-thumbnail">

	<?php the_post_thumbnail(); ?>

</figure>

<?php endif; ?>

<h1><?php the_title(); ?></h1>


</header> <!-- end entry-header -->

<!-- Article Content -->
<div class="entry-content">

<?php the_content(); ?>

<?php wp_link_pages(); ?>

</div><!-- end entry-content -->

<!-- Article Footer -->
<footer class="entry-footer">
	<?php

if(is_user_logged_in()){
					echo '<p>';
					edit_post_link(__('Edit','mi-framework'),'<span class="meta-edit">','</span>');
					echo '</p>';
				}


	?>

</footer><!-- end entry-footer -->


</article>
<?php comments_template(); ?>

<?php endwhile; ?>

<!-- end article -->
</div><!-- end main-content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>