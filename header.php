<?php
/**
* header.php
*
* The header of the theme.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
*/
 ?>
 <!DOCTYPE HTML>
 <!-- [if IE 8]<html <?php language_attributes(); ?> class="ie8"><![endif] -->
 <!-- [if !IE]<!--><html <?php language_attributes(); ?>><!--<![endif] -->
<html>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<!-- Mobile Specfic Meta -->
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
		<!-- stylesheet -->


<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
<div id="wrapper">
   <?php  if($mi_framework_opt['loader'] != 'yes'){ ?>
<!--// loader -->
<div id ="mi-loader">
  <div class ="loader"></div>
</div>
<!-- // loader end -->
   <?php } ?>

   <!-- // being topheader -->
        <div id="secondary-header">
            <div class="container">
                <div class="row">
                    <!-- start of header left -->
                     <?php if(is_active_sidebar('header_top_left')):  ?>
                    <div class="col secondary-left-bar">
            <?php dynamic_sidebar('header_top_left'); ?>
                    <!-- end of header left -->
                    </div>
                    <?php endif; ?>
                     <?php if(is_active_sidebar('header_top_left')):  ?>
                    <div class="col secondary-right-bar">
                <?php dynamic_sidebar('header_top_right'); ?>
                    <!-- end of header left -->
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>


   <!-- header -->
	<!-- header -->
    <header id="header-default" class="site-header" >
<nav class="navbar   navbar-light bg-light navbar navbar-expand-sm  sticky-tops">
    <div class="container header-contents">
      
        <div class="site-logo">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><?php echo bloginfo('name'); ?></a>
    </div>
          <!-- end of site-logo -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="site-navigation ml-md-auto" role="navigation">
         <?php mi_main_menu(); ?>
  </div>
    </div>
  <!-- end of container -->
  </nav><!-- end of nav -->
  </header>
  <!-- end site-header -->
