<?php
/**
 * functions.php
 *
 * The theme's functions and definitions.
 */


/**
 * ----------------------------------------------------------------------------------------
 * 1.0 - Define constants.
 * ----------------------------------------------------------------------------------------
 */
define( 'THEMEROOT', get_template_directory_uri() );
define( 'IMAGES', THEMEROOT . '/images/' );
define( 'SCRIPTS', THEMEROOT . '/js/' );
define('FRAMEWORK', get_template_directory(). '/inc/');
define( 'THEME_NAME', 'mi-framework' );
define( 'THEME_VERSION', '2.0' );
global $mi_framework_opt;
/**
 * ----------------------------------------------------------------------------------------
 * 2.0 - Load the FILES.
 * ----------------------------------------------------------------------------------------
 */

include FRAMEWORK . 'init.php';
include FRAMEWORK . 'cleanup.php';
include FRAMEWORK . 'enqueue.php';
include FRAMEWORK . 'template-tags.php';
/**
 * ----------------------------------------------------------------------------------------
 * 3.0 - Setup the content value based on theme's design.
 * ----------------------------------------------------------------------------------------
 */
if (!isset($content_width)) {
	$content_width = 1170;
}

/**
 * ----------------------------------------------------------------------------------------
 * 4.0 - Setup the content value based on theme's design.
 * ----------------------------------------------------------------------------------------
 */

if (! function_exists('mi_setup')) {
	function mi_setup(){
	/**
 	 * Make the theme available for translation.
     */
 		/**
		 * Add support for post formats.
		 */
		add_theme_support( 'post-formats',
			array(
				'gallery',
				'link',
				'image',
				'quote',
				'video',
				'audio'
			)
		);

		/**
		 * Add support for automatic feed links.
		 */
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Add support for post thumbnails.
		 */
		add_theme_support( 'post-thumbnails' );

		/**
	     * Add support for WooComerce.
		 */
	 add_theme_support( 'woocommerce' );

/**
	     * Add support for custom-header.
		 */
	 add_theme_support( 'custom-header' );
/**
	     * Add support for custom-background.
		 */
	 add_theme_support( "custom-background" );
/**
	     * Add support for custom-background.
		 */
	add_editor_style( array( 'css/editor-style.css' ) );
   /**
      * Add support for Title.
    */
   add_theme_support( "title-tag" );
/**
      * Add support for menu.
    */
   add_theme_support( "menu" );
		/**
		 * Register nav menus.
		 */
		register_nav_menus(
			array(
				'main-menu' => __( 'Parimay Menu', 'mi-framework' ),
       			 'secondary-menu' => __( 'Secondary Menu', 'mi-framework' ),
				'footer-menu' => __( 'Footer Menu', 'mi-framework' )
			)
		);
	}

	add_action("after_setup_theme",'mi_setup');
}

/*add_filter('wp_nav_menu_items','add_to_second_menu', 10, 2);
function add_to_second_menu( $items, $args ) {
    if( $args->theme_location == 'main-menu')  {
        $items .= '<li class="serach" id="searchbar"><i class="fa fa-search"></i></li>';
        $items .='<div class="search-bar">
        <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2">'.get_search_form('home').'  </div></div></div></div>';
    }
    return $items;
}*/

/**
 * ----------------------------------------------------------------------------------------
 * 5.0 - Display meta information for a specific post.
 * ----------------------------------------------------------------------------------------
 */
if(! function_exists('mi_post_meta')){
	function mi_post_meta(){
		echo '<ul class="list-inline entry-meta">';
		if(get_post_type()==='post'){
			// If the post is sticky, mark it.

			if(is_sticky()){
				echo '<li class="meta-featured-post"><i class="fa fa-thumb-tack"></i> '. __('Sticky','mi-framework').'</li>';
			}
			//  Get the post author
			printf(
				'<li class="meta-author"><a href="%1$s" rel="author">%2$s</a></li>',
				esc_url(get_author_posts_url(get_the_author_meta('ID'))),
				get_the_author()
				);

			// Get the date.
			echo '<li class="meta-date">'. get_the_date() .'</li>';

			// The Categories
			$Categories_list = get_the_category_list(', ');
			if($Categories_list){

				echo '<li class="meta-categories">'. $Categories_list  .'</li>';
			}

			// The Tags
			$tag_list = get_the_tag_list('', ', ');
			if($tag_list){

				echo '<li class="meta-tags">'. $tag_list  .'</li>';
			}

			// comment link
			if(comments_open()):
				echo '<li>';
				echo '<span>';
				comments_popup_link(__('leve a comments','mi-framework'),__('One Comment so far','mi-framework'),__('View all % comments','mi-framework'));
				echo '</span>';
				echo '</li>';

				endif;

				// Edit Link
				if(is_user_logged_in()){
					echo '<li>';
					edit_post_link(__('Edit','mi-framework'),'<span class="meta-edit">','</span>');
					echo '</li>';
				}

		}
		echo '</ul>';
	}

}



/**
 * ----------------------------------------------------------------------------------------
 * 6.0 - Display navigation to the next/previous set of post.
 * ----------------------------------------------------------------------------------------
 */

if(!function_exists('mi_paging_nav')){
	function mi_paging_nav(){?>
	<ul>
		<?php if (get_previous_posts_link()):?>
	<li class="next">
		<?php previous_posts_link(__('Newer Posts &rarr;','mi-framework')); ?>
	</li>
	<?php endif; ?>

	<!-- post_post_link -->
		<?php if (get_next_posts_link()):?>
	<li class="previous">
		<?php next_posts_link(__(' &larr; older Posts','mi-framework')); ?>
	</li>
	<?php endif; ?>

	</ul>
	<?php
	}
}


/**
 * ----------------------------------------------------------------------------------------
 * 8.0 - Function that validates a field's length.
 * ----------------------------------------------------------------------------------------
 */
if (!function_exists('mi_validate_length')) {
	function mi_validate_length($fieldValue, $minLength){
		// First, remove trailing and leading whitespace
		return(strlen( trim( $fieldValue ) ) > $minLength);
	}
}


/* ---------------------------------------------------------------------------
 * Styles | Minify
 * --------------------------------------------------------------------------- */
if( ! function_exists( 'mi_styles_minify' ) )
{
	function mi_styles_minify( $css ){
		
		// remove comments
		$css = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css );	
		
		// remove whitespace
		$css = str_replace( array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css );
		
		return $css;
	}
}
/* ---------------------------------------------------------------------------
 * Styles | Dynamic
 * --------------------------------------------------------------------------- */
if( ! function_exists( 'styles_dynamic' ) )
{
	function styles_dynamic()
	{
		echo '<!-- style | dynamic -->'."\n";
		echo '<style id="mi-dnmc-style-css">'."\n";

			ob_start();
				// Style PHP

				include_once THEMEROOT . '/css/style.php';
	

			$css = ob_get_contents();
			
			ob_get_clean();
			
// 			echo $css;
			
			echo mi_styles_minify( $css ) ."\n";
	
		echo '</style>'."\n";
	}
}
//add_action( 'wp_head', 'styles_dynamic' );

function showMessageAdmin(){
	include_once (ABSPATH.'wp-admin/includes/plugin.php');
	if(!is_plugin_active( 'mi-carousel-slider' )):
echo '<div id="alert" class="alert error">';
echo '<p>This theme required to install <a href="https://wordpress.org/plugins/mi-carousel-slider/">MI Carousel Slider</a></p>';
echo '</div>';
endif;
}
add_action('admin_notices','showMessageAdmin');
?>
