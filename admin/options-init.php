<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "mi_framework_opt";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'mi_framework_opt',
        'dev_mode'=>FALSE,
        'use_cdn' => TRUE,
        'admin_bar_icon'       => 'dashicons-portfolio',
        'display_name'         => $theme->get( 'Name' ),
        'display_version'      => $theme->get( 'Version' ),
        'page_slug' => 'mi-theme-options',
        'page_title' => 'MI Theme Options',
       
        'update_notice' => FALSE,
        'admi-frameworkn_bar' => TRUE,
        'menu_type' => 'submenu',
        'menu_title' => 'Theme Options',
        'allow_sub_menu' => FALSE,
        //'page_parent_post_type' => 'your_post_type',
        'customi-frameworkzer' => TRUE,
        'default_mark' => '*',
        'google_api_key' => 'AIzaSyCVdqzChDF9jRtsZfQkHKv6H46zrd9n1M8',
        'dev_mode'=>true,
        'customizer'=>true,
        'hints' => array(
            'icon_position' => 'right',
            'icon_color' => 'lightgray',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),

        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'save_defaults' => TRUE,
        'show_import_export' => false,
        'database' => 'options',
        'transient_time'   => 60 * MINUTE_IN_SECONDS,
        'network_sites' => TRUE,
        'page_parent'      => 'mi_panel',
        'page_permissions' => 'manage_options',
        'menu_icon'        => '',
        'last_tab'         => '',
        'page_icon'        => 'icon-themes',
       
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/mi-dexigner',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/midexigner',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.twitter.com/midexigner',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://linkedin.com/in/midexigner',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'mi-framework' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'mi-framework' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'mi-framework' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'mi-framework' )
        )
    );
    //Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'mi-framework' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /* General Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'General', 'mi-framework' ),
        'id'     => 'general-tab',
        'icon'   => 'el el-home',
        'fields' => array(
           array(
    'id'       => 'body-background',
    'type'     => 'background',
    'title'    => __('Body Background Styling', 'mi-framework'),
    'subtitle' => __('Body background with image, color, etc.', 'mi-framework'),
    'desc'     => __('Change the background style.
.', 'mi-framework'),
    'default'  => array(
        'background-color' => '#ffffff',
    )
),array(
    'id'       => 'link-color',
    'type'     => 'link_color',
    'title'    => __('Links Color Option', 'mi-framework'),
    'subtitle' => __('Change the color of link, hover and active.', 'mi-framework'),
    'desc'     => __('This is the description field, again good for additional info.', 'mi-framework'),
    'default'  => array(
        'regular'  => '#1e73be', // blue
        'hover'    => '#dd3333', // red
        'active'   => '#8224e3',  // purple
        'visited'  => '#8224e3',  // purple
    )
),
array(
    'id'          => 'opt-typography',
    'type'        => 'typography',
    'title'       => __('Typography', 'mi-framework'),
    'google'      => true,
    'font-backup' => true,
    'output'      => array('h2'),
    'units'       =>'px',
    'subtitle'    => __('Typography option with each property can be called individually.', 'mi-framework'),
    'default'     => array(
        'color'       => '#333',
        'font-style'  => '400',
        'font-fami-frameworkly' => 'Abel',
        'google'      => true,
        'font-size'   => '33px',
        'line-height' => '40'
    ),
),array(
    'id'             => 'opt-spacing',
    'type'           => 'spacing',
    'output'         => array('.site-header'),
    'mode'           => 'margin',
    'units'          => array('em', 'px'),
    'units_extended' => 'false',
    'title'          => __('Padding/Margin Option', 'mi-framework'),
    'subtitle'       => __('Allow your users to choose the spacing or margin they want.', 'mi-framework'),
    'desc'           => __('You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'mi-framework'),
    'default'            => array(
        'margin-top'     => '0',
        'margin-right'   => '0',
        'margin-bottom'  => '0',
        'margin-left'    => '0',
        'units'          => 'em',
    )
)
        )
    ) );
/* General End */

 
    /* Header Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Header', 'mi-framework' ),
        'id'     => 'header-tab',
        'icon'   => 'el el-align-left',
        //'fields' => array()
    ) );
    
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Presets', 'mi-framework' ),
        'desc'       => __( '** You can quick change between header presets here.<br/> 
        This works best on a fresh install or resetted options. <br/>
        You can safely try new layouts without loosing changes. <br/>
        Just remember to NOT save if you do not want to use the selected preset.**', 'mi-framework' ) ,
        'id'         => 'presets',
        'subsection' => true,
        'icon'   => 'el el-brush ',
        'fields'     => array(
            array(
                'id'       => 'header-layout',
                'type'     => 'image_select',
                'title'    => __('Main Layout', 'mi-framework'), 
                'subtitle' => __('Select main content and sidebar alignment. Choose between 1, 2 or 3 column layout.', 'mi-framework'),
                'options'  => array(
                    '1'      => array(
                        'alt'   => 'Header Default', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    '2'      => array(
                        'alt'   => 'Header Center', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    '3'      => array(
                        'alt'   => 'Header Dark', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    '4'      => array(
                        'alt'   => 'Header wide Nav', 
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                    '5'      => array(
                        'alt'   => 'Header Simple', 
                        'img'   => ReduxFramework::$_url.'assets/img/3cl.png'
                    ),
                    '6'      => array(
                        'alt'  => 'Header center with hum burger Menu', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cr.png'
                    ),
                    '7'      => array(
                        'alt'  => 'Header without Topbar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cr.png'
                    ),
                    '8'      => array(
                        'alt'  => 'Header Cart Top', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cr.png'
                    )
                ),
                'default' => '2'
            )
        )
    ) );


    /* Logo Being */
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Logo', 'mi-framework' ),
        'id'     => 'logo-tab',
        'icon'   => 'el el-brush ',
        'subsection' => true,
        'fields' => array(
        array(
    'id'       => 'logo-normal',
    'type'     => 'media',
    'url'      => true,
    'title'    => __('Logo Image - normal', 'mi-framework'),
    'subtitle'     => __('Choose a default logo image to display', 'mi-framework'),
    'default'  => array(
          'url'=> get_template_directory_uri() . '/images/logo.png',
    ),

),  array(
    'id'       => 'logo-sticky',
    'type'     => 'media',
    'url'      => true,
    'title'    => __('Logo Image - Sticky Header', 'mi-framework'),
    'subtitle'     => __('Choose a logo image to display for "Sticky" header type', 'mi-framework'),
    'default'  => array(
        'url'=> get_template_directory_uri() . '/images/logo.png',
    ),

),
 array(
    'id'       => 'favicon-icon',
    'type'     => 'media',
    'url'      => true,
    'title'    => __('Favicon Icon', 'mi-framework'),
    'subtitle'     => __('Click Upload or Enter the direct path to your favicon. For example //your_website_url_here/wp-content/themes/themeXXXX/favicon.ic', 'mi-framework'),
    'default'  => array(
        'url'=> get_template_directory_uri() . '/images/icons/mi-framework-icon.png',
    ),

),
    ),
    ) );
/* Logo End */
/* Top Bar Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Top Bar', 'mi-framework' ),
    'id'     => 'topbar-tab',
    'icon'   => 'el el-brush ',
    'subsection' => true,
    'fields' => array()
)
);
/* Top Bar End */
/* Header Main Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Header Main', 'mi-framework' ),
    'id'     => 'header-main-tab',
    'icon'   => 'el el-brush ',
    'subsection' => true,
    'fields' => array()
)
);
/* Header Main End */
/* Navigation Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Navigation', 'mi-framework' ),
    'id'     => 'navigation-tab',
    'icon'   => 'el el-brush ',
    'subsection' => true,
    'fields' => array()
)
);
/* Navigation End */
/* Header Mobile Menu Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Header Mobile Menu ', 'mi-framework' ),
    'id'     => 'header-mobile-menu-tab',
    'icon'   => 'el el-brush ',
    'subsection' => true,
    'fields' => array()
)
);
/* Header Mobile Menu  End */
/* Sticky Header Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Sticky Header', 'mi-framework' ),
    'id'     => 'sticky-header-tab',
    'icon'   => 'el el-brush ',
    'subsection' => true,
    'fields' => array()
)
);
/* Sticky Header End */
/* Header End */
/* Color Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Color', 'mi-framework' ),
    'id'     => 'color-tab',
    'icon'   => 'el el-brush ',
    'fields' => array(
     array(
         'id'       => 'body-color',
         'type'     => 'color',
         'title'    => __('Body Background Color', 'mi-framework'), 
         'subtitle' => __('Pick a background color for the theme (default: #fff).', 'mi-framework'),
         'default'  => '#FFFFFF',
         'validate' => 'color',
     ),
     array(
         'id'       => 'button-color',
         'type'     => 'color',
         'title'    => __('Button Color', 'mi-framework'), 
         //'subtitle' => __('Pick a Button color for the theme (default: #fff).', 'mi-framework'),
         'default'  => '#FFFFFF',
         'validate' => 'color',
     ),
     array(
         'id'       => 'button-color-hover',
         'type'     => 'color',
         'title'    => __('Button Color Hover', 'mi-framework'), 
         //'subtitle' => __('Pick a Button color for the theme (default: #fff).', 'mi-framework'),
         'default'  => '#FFFFFF',
         'validate' => 'color',
     ),

),
) );
/* Color End */

 /* Footer Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Footer', 'mi-framework' ),
        'id'     => 'footer-tab',
        'icon'   => 'el el-align-center',
        //'fields' => array()
    ) );
/*Footer Block Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Footer Column', 'mi-framework' ),
    'desc'       => __( '** You can quick change between header presets here.<br/> 
        This works best on a fresh install or resetted options. <br/>
        You can safely try new layouts without loosing changes. <br/>
        Just remember to NOT save if you do not want to use the selected preset.**', 'mi-framework' ) ,
    'id'     => 'footer-column-tab',
    'subsection' => true,
    'icon'   => 'el el-brush ',
    'fields' => array(
        array(
            'id'       => 'footer-layout',
            'type'     => 'image_select',
            'title'    => __('Main Layout', 'redux-framework-demo'), 
            'subtitle' => __('Select main content and sidebar alignment. Choose between 1, 2 or 3 column layout.', 'redux-framework-demo'),
            'options'  => array(
                '1'      => array(
                    'alt'   => '1 Column', 
                    'img'   => ReduxFramework::$_url.'assets/img/1cols.png'
                ),
                '2'      => array(
                    'alt'   => '2 Column Left', 
                    'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                ),
                '3'      => array(
                    'alt'   => '2 Column Right', 
                    'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                ),
                '4'      => array(
                    'alt'   => '3 Column Middle', 
                    'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                ),
                '5'      => array(
                    'alt'   => '3 Column Left', 
                    'img'   => ReduxFramework::$_url.'assets/img/3cl.png'
                ),
                '6'      => array(
                    'alt'  => '3 Column Right', 
                    'img'  => ReduxFramework::$_url.'assets/img/3cr.png'
                )
            ),
            'default' => '2'
        ) 
    )
)
);
/*  Footer Coloumn End */
/*Copyright Text Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Copyright Text', 'mi-framework' ),
    'id'     => 'copyright-text-tab',
    'subsection' => true,
    'icon'   => 'el el-brush ',
    'fields' => array()
)
);
/*  Copyright Text End */
/* Back To Top Button Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Back To Top Button', 'mi-framework' ),
    'id'     => 'back-to-top--tab',
    'icon'   => 'el el-brush ',
    'subsection' => true,
    'fields' => array()
)
);
/*  Back To Top Button End */
/* Footer End */

 /* Title Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Title', 'mi-framework' ),
        'id'     => 'title-tab',
        'icon'   => 'el el-edit',
        'fields' => array()
    ) );
/* Title End */
/* Portfolio Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Portfolio', 'mi-framework' ),
    'id'     => 'portfolio-tab',
    'icon'   => 'el el-picture',
    'fields' => array()
) );
/* Portfolio End */

 /* Elements Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Elements', 'mi-framework' ),
        'id'     => 'elements-tab',
        'icon'   => 'el el-flag ',
        'fields' => array(

        )
    ) );
/* Elements End */
/* Sidebar Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Sidebar', 'mi-framework' ),
        'id'     => 'sidebar-tab',
        'icon'   => 'el el-align-justify',
        'fields' => array()
    ) );
/* Sidebar End */


/* Pages Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Pages', 'mi-framework' ),
        'id'     => 'pages-tab',
        'icon'   => 'el el-file-new',
        'fields' => array()
    ) );
/* Pages End */

/* Blog Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Blog', 'mi-framework' ),
        'id'     => 'blog-tab',
        'icon'   => 'el el-book ',
        'fields' => array()
    ) );
/* Blog End */


/* 404 Error Page Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( '404 Error Page', 'mi-framework' ),
        'id'     => 'four-zero-four-tab',
        'icon'   => 'el el-remove',
        'fields' => array()
    ) );
/* 404 Error Page End */
/* Contact Page Being */
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Contact Page', 'mi-framework' ),
        'id'     => 'contact-tab',
        'icon'   => 'el el-file',
        'fields' => array()
    ) );
/* Contact Page End */
/* Maintenance Mode Being */

      Redux::setSection( $opt_name, array(
        'title'  => __( 'Maintenance Mode', 'mi-framework' ),
        'id'     => 'maintenance-tab',
        'icon'   => 'el el-wrench',
        'fields' => array(
        array(
        'id'       => 'opt-switch',
        'type'     => 'switch',
        'title'    => __('Switch On', 'mi-framework'),
        'subtitle' => __('Turn on this option if you want to enable maintenance mode on your site', 'mi-framework'),
        'default'  => false,),
        array(
                'id'       => 'featured_post_type',
                'type'     => 'select',
                'multi'    => false,
                'hidden'   => ( $options['test_value'] == 1 ) ? true : false,
                'data'      => 'pages',
                'args' => array('post_type' => 'page' ),
                'title'    => __('Maintenance Page','mi-framework' ),
                'subtitle' => __('Selected post will be displayed in page top menu', 'mi-framework'),
                //'desc'     => __('Page will be marked as front for this post type', 'mi-framework'),
            ),
    )
    ) );
/* Maintenance Mode End */
// https://docs.reduxframework.com/extensions/repeater/
/* Custom CSS Being */
Redux::setSection( $opt_name, array(
    'title'  => __( 'Custom CSS', 'mi-framework' ),
    'id'     => 'custom-css-tab',
    'icon'   => 'el el-file',
    'fields' => array(
        array(
            'id'       => 'css_editor',
            'type'     => 'ace_editor',
            'title'    => __('CSS Code', 'mi-framework'),
            'subtitle' => __('Paste your CSS code here.', 'mi-framework'),
            'mode'     => 'css',
            'theme'    => 'monokai',
            'desc'     => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
            //'default'  => "#header{\nmargin: 0 auto;\n}"
        ),

    )
) );
/* Custom CSS End */


  /*  Redux::setSection( $opt_name, array(
        'title'  => __( 'Basic Field', 'mi-framework' ),
        'id'     => 'basic',
        'desc'   => __( 'Basic field with no subsections.', 'mi-framework' ),
        'icon'   => 'el el-home',
        'fields' => array(
            array(
                'id'       => 'opt-text',
                'type'     => 'text',
                'title'    => __( 'Example Text', 'mi-framework' ),
                'desc'     => __( 'Example description.', 'mi-framework' ),
                'subtitle' => __( 'Example subtitle.', 'mi-framework' ),
            )
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __( 'Basic Fields', 'mi-framework' ),
        'id'    => 'basic',
        'desc'  => __( 'Basic fields as subsections.', 'mi-framework' ),
        'icon'  => 'el el-home'
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Text', 'mi-framework' ),
        'desc'       => __( 'For full documentation on this field, visit: ', 'mi-framework' ) . '<a href="http://docs.reduxframework.com/core/fields/text/" target="_blank">http://docs.reduxframework.com/core/fields/text/</a>',
        'id'         => 'opt-text-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'text-example',
                'type'     => 'text',
                'title'    => __( 'Text Field', 'mi-framework' ),
                'subtitle' => __( 'Subtitle', 'mi-framework' ),
                'desc'     => __( 'Field Description', 'mi-framework' ),
                'default'  => 'Default Text',
            ),
        )
    ) );

   Redux::setSection( $opt_name, array(
        'title'      => __( 'Text Area', 'mi-framework' ),
        'desc'       => __( 'For full documentation on this field, visit: ', 'mi-framework' ) . '<a href="http://docs.reduxframework.com/core/fields/textarea/" target="_blank">http://docs.reduxframework.com/core/fields/textarea/</a>',
        'id'         => 'opt-textarea-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'textarea-example',
                'type'     => 'textarea',
                'title'    => __( 'Text Area Field', 'mi-framework' ),
                'subtitle' => __( 'Subtitle', 'mi-framework' ),
                'desc'     => __( 'Field Description', 'mi-framework' ),
                'default'  => 'Default Text',
            ),
        )
    ) ); */

    /*
     * <--- END SECTIONS
     */
