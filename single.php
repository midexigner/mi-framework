<?php 
/**
* single.php
*
* The template for displaying single posts.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>


 <?php get_header();  ?>

<!-- content pages area -->
<div class="main-content col-md-8" rol="main">

<?php while(have_posts()):the_post(); ?>
	
	<?php get_template_part('content',get_post_format()); ?>

	<?php comments_template(); ?>

	<?php endwhile; ?>

</div><!-- end main-content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>