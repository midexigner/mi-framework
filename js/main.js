(function ($) {
"use strict";
$('[href="#"]').attr("href","javascript:;");
 $(window).on('load', function() {
    $("#mi-loader").delay(1000).fadeOut("slow");
    });
// meanmenu
$('#mobile-menu').meanmenu({
	meanMenuContainer: '.mobile-menu',
	meanScreenWidth: "992"
});
// custom scrollbar
// 
$("body").niceScroll({
	styler:"fb",
  cursorcolor:"#7DBA2F", // change cursor color in hex
  cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
   cursorborderradius: "5px", // border radius in pixel for cursor 
   background: '#404040',// change css for rail background
    cursorborder: '',// css definition for cursor border : 1px solid #fff
     autohidemode: false,// how hide the scrollbar works, possible values: 
     smoothscroll: true, // scroll with ease movement
});

  
// One Page Nav
var top_offset = $('.header-area').height() - 10;
$('.main-menu nav ul').onePageNav({
	currentClass: 'active',
	scrollOffset: top_offset,
});


$(window).on('scroll', function () {
	var scroll = $(window).scrollTop();
	if (scroll < 245) {
		$(".header-sticky").removeClass("sticky");
	} else {
		$(".header-sticky").addClass("sticky");
	}
});



// mainSlider
function mainSlider() {
	var BasicSlider = $('.slider-active');
	BasicSlider.on('init', function (e, slick) {
		var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
		doAnimations($firstAnimatingElements);
	});
	BasicSlider.on('beforeChange', function (e, slick, currentSlide, nextSlide) {
		var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
		doAnimations($animatingElements);
	});
	BasicSlider.slick({
		autoplay: false,
		autoplaySpeed: 10000,
		dots: false,
		fade: true,
		arrows: false,
		responsive: [
			{ breakpoint: 767, settings: { dots: false, arrows: false } }
		]
	});

	function doAnimations(elements) {
		var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		elements.each(function () {
			var $this = $(this);
			var $animationDelay = $this.data('delay');
			var $animationType = 'animated ' + $this.data('animation');
			$this.css({
				'animation-delay': $animationDelay,
				'-webkit-animation-delay': $animationDelay
			});
			$this.addClass($animationType).one(animationEndEvents, function () {
				$this.removeClass($animationType);
			});
		});
	}
}
mainSlider();


// owlCarousel
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
	items:1,
	navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    nav:true,
	dots:false,
    responsive:{
        0:{
            items:1
        },
        767:{
            items:3
        },
        992:{
            items:5
        }
    }
})


/* magnificPopup img view */
$('.popup-image').magnificPopup({
	type: 'image',
	gallery: {
	  enabled: true
	}
});

/* magnificPopup video view */
$('.popup-video').magnificPopup({
	type: 'iframe'
});


// isotop
$('.grid').imagesLoaded( function() {
	// init Isotope
	var $grid = $('.grid').isotope({
	  itemSelector: '.grid-item',
	  percentPosition: true,
	  masonry: {
		// use outer width of grid-sizer for columnWidth
		columnWidth: '.grid-item',
	  }
	});
});

// filter items on button click
$('.portfolio-menu').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});

//for menu active class
$('.portfolio-menu button').on('click', function(event) {
	$(this).siblings('.active').removeClass('active');
	$(this).addClass('active');
	event.preventDefault();
});




// scrollToTop
$.scrollUp({
	scrollName: 'scrollUp', // Element ID
	topDistance: '300', // Distance from top before showing element (px)
	topSpeed: 300, // Speed back to top (ms)
	animation: 'fade', // Fade, slide, none
	animationInSpeed: 200, // Animation in speed (ms)
	animationOutSpeed: 200, // Animation out speed (ms)
	scrollText: '<i class="icofont icofont-long-arrow-up"></i>', // Text for element
	activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
});

// WOW active
new WOW().init();

$('#wrapper').imagesLoaded()
  .always( function( instance ) {
    console.log('all images loaded');
  })
  .done( function( instance ) {
    console.log('all images successfully loaded');
  })
  .fail( function() {
    console.log('all images loaded, at least one is broken');
  })
  .progress( function( instance, image ) {
    var result = image.isLoaded ? 'loaded' : 'broken';
    console.log( 'image is ' + result + ' for ' + image.img.src );
  });

function imgSet(){
	$('[data-imgurl]').each(function() { 
		var $this = $(this),
		ele = $this.attr('src'),
		attData = $this.data('imgurl');

		if(ele !== undefined){
			$this.attr('src', attData);
		}else{
			$this.css({
				"background-image": 'url('+ attData + ')'
			});
		}
	});
}
imgSet();
imgHoverSet();
function imgHoverSet(){
	$('[data-imgurl-hover]').each(function() { 
		var $this = $(this),
		attData = $this.data('imgurl-hover'),
		attDatas = $this.data('imgurl');
		 $this.hover(
    function() {
       $this.css({
				"background-image": 'url('+ attData + ')'
			});
    },
    function() {
       $this.css({
				"background-image": 'url('+ attDatas + ')'
			});
    }
 );
	
});
}

})(jQuery);

/*

<script>
		var size = jQuery('.widget-social-icon span a').attr('data-size');
		var color = jQuery('.widget-social-icon span a').attr('data-color');
		var color_hover = jQuery('.widget-social-icon span a').attr('data-color-hover');
		var bgcolor = jQuery('.widget-social-icon span a').attr('data-bgcolor');
		var bgcolor_hover = jQuery('.widget-social-icon span a').attr('data-bgcolor-hover');
		var border_width = jQuery('.widget-social-icon span a').attr('data-border_width');
		var border_radius = jQuery('.widget-social-icon span a').attr('data-border-radius');
		var border_color = jQuery('.widget-social-icon span a').attr('data-border-color');
		var border_color_hover = jQuery('.widget-social-icon span a').attr('data-border-color-hover');
		var padding = jQuery('.widget-social-icon span a').attr('data-padding');
		var margin = jQuery('.widget-social-icon span a').attr('data-margin');
		//alert(size);
// 		.css({
//    'font-size' : '10px',
//    'width' : '30px',
//    'height' : '10px'
// });
//alert(color);
jQuery('.widget-social-icon span a').css({
	"font-size":	size,
	"color": 		color,
	"background-color": 	bgcolor,
	"border-width": 	border_width,
	"border-radius": 	border_radius,
	"border-color": 	border_color,
	"padding": 	padding,
	"margin": 	margin,
});
jQuery(".widget-social-icon span a").hover(
    function() {
			//alert();
        //mouse over
				
				$(this).css({
					"color": 		color_hover,
					"background-color": 	bgcolor,
					"border-color": 	border_color,
				
				})
    }, function() {
        //mouse out
				 $(this).css({
				 	"color": 		"",
				 	"background-color": 	"",
					"border-color": 	"",
				 })
				

    });
		</script>

 */