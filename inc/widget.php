<?php
/**
 * ----------------------------------------------------------------------------------------
 * 7.0 - Register the Widegets areas.
 * ----------------------------------------------------------------------------------------
 */



function create_widget($name, $id, $description) {

	register_sidebar(array(
		'name' => __( $name ),
		'id' => $id,
		'description' => __( $description ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div> <!-- end widget -->',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>'
	));

}

// Create widgets in the footer
create_widget( 'Sidebar', 'sidebar', 'Default Sidebar' );
create_widget("Sidebar Page", "sidebar_page", "Sidebar for Page");
create_widget("Header Top Left", "header_top_left", "Header Top Left");
create_widget("Header Top Right", "header_top_right", "Header Top Right");
create_widget( 'Footer Column 1', 'footer_column_1', 'Footer Column 1' );
create_widget( 'Footer Column 2', 'footer_column_2', 'Footer Column 2' );
create_widget( 'Footer Column 3', 'footer_column_3', 'Footer Column 3' );
create_widget( 'Footer Column 4', 'footer_column_4', 'Footer Column 4' );
create_widget( 'Footer Bottom Left', 'footer_bottom_left', 'Footer Bottom Left' );
create_widget( 'Footer Bottom Center', 'footer_bottom_center', 'Footer Bottom Center' );
create_widget( 'Footer Bottom Right', 'footer_bottom_right', 'Footer Bottom Right' );
create_widget( 'Shop', 'shop', 'Show in Shop Page' );

include FRAMEWORK .'widgets/widget-business-hours.php';
include FRAMEWORK .'widgets/widget-contact-info.php';
include FRAMEWORK .'widgets/widget-social-icon.php';


 ?>
