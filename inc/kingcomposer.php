<?php
$kc->add_map(
    array(
        'mi_slide' => array(
            'name'        => 'MI Slide',
            'description' => __('MI Slider', 'mi-framework'),
            'icon'        => 'sl-file-image',
            'category'    => '',
            'params'      => array(
                array(
                    'name'  => 'mi-slide',
                    'label' => '',
                    'number_slider'  => 'text',
                )
            )
        )
    )
);
add_shortcode( 'mi_slide', 'mi_slider' );

// avoid error when inactive KingComposer
if (function_exists('kc_prebuilt_template')) {
    $xml_path = get_template_directory().'/package.xml';
    kc_prebuilt_template('Twenty Templates', $xml_path);
}