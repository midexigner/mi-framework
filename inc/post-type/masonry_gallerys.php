<?php
/* Create Custom Post Type : Masonry Gallery */
function create_masonry_gallery_post_type() 
{
	$labels = array(
		'name' => __( 'Masonry Gallery','mi-framework'),
		'singular_name' => __( 'Masonry Gallery','mi-framework' ),
		'add_new' => __('Add New','mi-framework'),
		'add_new_item' => __('Add New Masonry Gallery','mi-framework'),
		'edit_item' => __('Edit Masonry Gallery','mi-framework'),
		'new_item' => __('New Masonry Gallery','mi-framework'),
		'view_item' => __('View Masonry Gallery','mi-framework'),
		'search_items' => __('Search Masonry Gallery','mi-framework'),
		'not_found' =>  __('No Masonry Gallery found','mi-framework'),
		'not_found_in_trash' => __('No Portfolio found in Trash','mi-framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'query_var' => true,
		'capability_type' => 'page',
		'hierarchical' => false,
		'menu_icon' => ( version_compare( $GLOBALS['wp_version'], '3.8', '>=' ) ) ? 'dashicons-images-alt2' : '',
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail')
	  ); 
	  
	  register_post_type('mi_masonry_gallery',$args);
}

add_action( 'init', 'create_masonry_gallery_post_type' );


add_action( 'init', 'create_masonry_taxonomy' );

function create_masonry_taxonomy() {
	register_taxonomy(
		'mi_masonry_cat',
		'mi_masonry_gallery',
		array(
			'label' => __( 'Masonry Categories' ),
			'rewrite' => array( 'slug' => 'sliders' ),
			'hierarchical' => true,
		)
	);
}

 ?>