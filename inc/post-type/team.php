<?php
/* Create Custom Post Type : Our Team */
function create_our_team_post_type() 
{
	$labels = array(
		'name' => __( 'Our Team','mi-framework'),
		'singular_name' => __( 'Our Team','mi-framework' ),
		'add_new' => __('Add New','mi-framework'),
		'add_new_item' => __('Add New Our Team','mi-framework'),
		'edit_item' => __('Edit Our Team','mi-framework'),
		'new_item' => __('New Our Team','mi-framework'),
		'view_item' => __('View Our Team','mi-framework'),
		'search_items' => __('Search Our Team','mi-framework'),
		'not_found' =>  __('No Our Team found','mi-framework'),
		'not_found_in_trash' => __('No Our Team found in Trash','mi-framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'query_var' => true,
        'menu_icon'           =>  ( version_compare( $GLOBALS['wp_version'], '3.8', '>=' ) ) ? 'dashicons-businessman' : '',
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail')
	  ); 
	  
	  register_post_type('mi_team',$args);
}

add_action( 'init', 'create_our_team_post_type' );

add_action( 'init', 'create_team_taxonomy' );

function create_team_taxonomy() {
	register_taxonomy(
		'mi_team_category',
		'mi_team',
		array(
			'label' => __( 'Team Categories' ),
			'rewrite' => array( 'slug' => 'mi_team_category' ),
			'hierarchical' => true,
		)
	);
}


 ?>