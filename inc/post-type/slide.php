<?php
/* Create Custom Post Type : Slide */
function create_slide_post_type() 
{
	$labels = array(
		'name' => __( 'MI Slides','mi-framework'),
		'singular_name' => __( 'MI Slide','mi-framework' ),
		'add_new' => __('Add New','mi-framework'),
		'add_new_item' => __('Add New MI Slide','mi-framework'),
		'edit_item' => __('Edit MI Slide','mi-framework'),
		'new_item' => __('New MI Slide','mi-framework'),
		'view_item' => __('View MI Slide','mi-framework'),
		'search_items' => __('Search MI Slide','mi-framework'),
		'not_found' =>  __('No MI Slide found','mi-framework'),
		'not_found_in_trash' => __('No MI Slide found in Trash','mi-framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'query_var' => true,
		'menu_icon' => ( version_compare( $GLOBALS['wp_version'], '3.8', '>=' ) ) ? 'dashicons-slides' : '',
		'menu_position' => 8,
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array('title','thumbnail')
	  ); 
	  
	  register_post_type('mi_slides',$args);
}

add_action( 'init', 'create_slide_post_type' );

add_action( 'init', 'create_slide_taxonomy' );

function create_slide_taxonomy() {
	register_taxonomy(
		'sliders',
		'mi_slides',
		array(
			'label' => __( 'Sliders' ),
			'rewrite' => array( 'slug' => 'sliders' ),
			'hierarchical' => true,
		)
	);
}

 ?>