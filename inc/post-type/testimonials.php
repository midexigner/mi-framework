<?php
/* Create Custom Post Type : Testimonials */
function create_testimonials_post_type() 
{
	$labels = array(
		'name' => __( 'Testimonials','mi-framework'),
		'singular_name' => __( 'Testimonial','mi-framework' ),
		'add_new' => __('Add New','mi-framework'),
		'add_new_item' => __('Add New Testimonials','mi-framework'),
		'edit_item' => __('Edit Testimonials','mi-framework'),
		'new_item' => __('New Testimonials','mi-framework'),
		'view_item' => __('View Testimonials','mi-framework'),
		'search_items' => __('Search Testimonials','mi-framework'),
		'not_found' =>  __('No Testimonials found','mi-framework'),
		'not_found_in_trash' => __('No Testimonials found in Trash','mi-framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'query_var' => true,
		'menu_icon' => ( version_compare( $GLOBALS['wp_version'], '3.8', '>=' ) ) ? 'dashicons-format-quote' : '',
		'capability_type' => 'page',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail')
	  ); 
	  
	  register_post_type('mi_testimonials',$args);
}

add_action( 'init', 'create_testimonials_post_type' );


add_action( 'init', 'create_testimonials_taxonomy' );

function create_testimonials_taxonomy() {
	register_taxonomy(
		'testimonials_categories',
		'mi_testimonials',
		array(
			'label' => __( 'Testimonials Categories' ),
			'rewrite' => array( 'slug' => 'testimonials_categories' ),
			'hierarchical' => true,
		)
	);
}

 ?>