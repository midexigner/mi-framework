<?php
/* Create Custom Post Type : FAQs */
function create_faqs_post_type() 
{
	$labels = array(
		'name' => __( 'FAQs','mi-framework'),
		'singular_name' => __( 'Faq','mi-framework' ),
		'add_new' => __('Add New','mi-framework'),
		'add_new_item' => __('Add New FAQs','mi-framework'),
		'edit_item' => __('Edit FAQs','mi-framework'),
		'new_item' => __('New FAQs','mi-framework'),
		'view_item' => __('View FAQs','mi-framework'),
		'search_items' => __('Search FAQs','mi-framework'),
		'not_found' =>  __('No FAQs found','mi-framework'),
		'not_found_in_trash' => __('No FAQs found in Trash','mi-framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'query_var' => true,
        'menu_icon'           => ( version_compare( $GLOBALS['wp_version'], '3.8', '>=' ) ) ? 'dashicons-editor-help' : '',
		'capability_type' => 'page',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail')
	  ); 
	  
	  register_post_type('mi_faqs',$args);
}

add_action( 'init', 'create_faqs_post_type' );

add_action( 'init', 'create_faqs_taxonomy' );

function create_faqs_taxonomy() {
	register_taxonomy(
		'faqs_scategories',
		'mi_faqs',
		array(
			'label' => __( 'FAQs Categories' ),
			'rewrite' => array( 'slug' => 'faqs_categories' ),
			'hierarchical' => true,
		)
	);
}

/* Label */
add_filter( 'manage_mi_faqs_posts_columns', 'mi_set_faqs_columns' );
add_action( 'manage_mi_faqs_posts_custom_column', 'mi_faqs_custom_column', 10, 2 );

// Set Custom Columns
function mi_set_faqs_columns( $columns){
  var_dump($columns);
#unset($columns['date']);
$newColumns = array();
$newColumns['title'] = 'Name';
$newColumns['shortcode'] = 'Shortcode';
$newColumns['category'] = 'Category';
$newColumns['date'] = 'Date';
return $newColumns;
}

//  Create Custom Columns

function mi_faqs_custom_column( $column, $post_id ){
  
  switch( $column ){
    
    case 'shortcode' :
    // shortcode column
    $cat = get_the_term_list($post->ID,'faqs_scategories');
    $cat = strip_tags( $cat );
    echo "<pre>[mi-faqs category='".$cat."']</pre>";
      break;
      
      case 'category' :
      // category column
      echo get_the_term_list($post->ID,'faqs_scategories');
      break;
  }
  
}
 ?>