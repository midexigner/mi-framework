<?php
/* Create Custom Post Type : Portfolios */
function create_portfolio_post_type() 
{
	$labels = array(
		'name' => __( 'Portfolios','mi-framework'),
		'singular_name' => __( 'Portfolio','mi-framework' ),
		'add_new' => __('Add New','mi-framework'),
		'add_new_item' => __('Add New Portfolios','mi-framework'),
		'edit_item' => __('Edit Portfolios','mi-framework'),
		'new_item' => __('New Portfolios','mi-framework'),
		'view_item' => __('View Portfolios','mi-framework'),
		'search_items' => __('Search Portfolios','mi-framework'),
		'not_found' =>  __('No Portfolios found','mi-framework'),
		'not_found_in_trash' => __('No Portfolio found in Trash','mi-framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'query_var' => true,
		'capability_type' => 'page',
		'hierarchical' => false,
		'menu_icon' => ( version_compare( $GLOBALS['wp_version'], '3.8', '>=' ) ) ? 'dashicons-images-alt2' : '',
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail')
	  ); 
	  
	  register_post_type('mi_portfolio',$args);
}

add_action( 'init', 'create_portfolio_post_type' );

// register two taxonomies to go with the post type
add_action( 'init', 'portfolio_create_taxonomies');
function portfolio_create_taxonomies() {
    // Portfolio Categories taxonomy
    $labels = array(
        'name'              => _x( 'Portfolio Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Portfolio Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Portfolio Categories' ),
        'all_items'         => __( 'All Portfolio Categories' ),
        'parent_item'       => __( 'Parent Portfolio Category' ),
        'parent_item_colon' => __( 'Parent Portfolio Category:' ),
        'edit_item'         => __( 'Edit Portfolio Category' ),
        'update_item'       => __( 'Update Portfolio Category' ),
        'add_new_item'      => __( 'Add New Portfolio Category' ),
        'new_item_name'     => __( 'New Portfolio Category' ),
        'menu_name'         => __( 'Portfolio Categories' ),
    );
    register_taxonomy(
        'miportfolio_categories',
        'mi_portfolio',
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'query_var' => true,
            'rewrite' => true,
            'show_admin_column' => true
        )
    );
    // Portfolio Tags taxonomy
    $labels = array(
        'name'              => _x( 'Portfolio Tags', 'taxonomy general name' ),
        'singular_name'     => _x( 'Portfolio Tag', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Portfolio Tags' ),
        'all_items'         => __( 'All Portfolio Tag' ),
        'parent_item'       => __( 'Parent Portfolio Tag' ),
        'parent_item_colon' => __( 'Parent Portfolio Tag:' ),
        'edit_item'         => __( 'Edit Portfolio Tag' ),
        'update_item'       => __( 'Update Portfolio Tag' ),
        'add_new_item'      => __( 'Add New Portfolio Tag' ),
        'new_item_name'     => __( 'New Portfolio Tag' ),
        'menu_name'         => __( 'Portfolio Tags' ),
    );
    register_taxonomy(
        'mi_portfolio_tags',
        'mi_portfolio',
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'query_var' => true,
            'rewrite' => true,
            'show_admin_column' => true
        )
    );
}



 ?>