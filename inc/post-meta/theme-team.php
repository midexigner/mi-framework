<?php

/*-----------------------------------------------------------------------------------*/
/*	Add Metabox to Team Personal Data
/*-----------------------------------------------------------------------------------*/
	add_action( 'add_meta_boxes', 'team_meta_box_add' );

	function team_meta_box_add()
	{
		add_meta_box( 'slide-meta-box', __('Personal Options', 'mi-framework'), 'team_meta_box', 'mi_team', 'normal', 'high' );
	}

	function team_meta_box( $post )
	{
		$values = get_post_custom( $post->ID );
		$team_position = isset( $values['team_position'] ) ? esc_attr( $values['team_position'][0] ) : '';
        $team_email = isset( $values['team_email'] ) ? esc_attr( $values['team_email'][0] ) : '';
        $team_info = isset( $values['team_info'] ) ? esc_attr( $values['team_info'][0] ) : '';
		wp_nonce_field( 'team_meta_box_nonce', 'meta_box_nonce_slide' );
		?>
<table style="width:100%;" class="form-table">
	<tr>
		<td colspan="2"><p>Please fill additional fields for person.</p>
			<hr></td>
	</tr>
  <tr>
    <th style="width:25%"><label for="team_position"><strong>
        <?php _e('Position','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Input position of the person.','mi-framework'); ?>
      </span></label>
        </th>
    <td><input type="text" name="team_position" id="team_position" style="width:70%; margin-right:4%;" value="<?php echo $team_position; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>
     <tr>
    <th><label for="team_email"><strong>
        <?php _e('Email','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Input email address.','mi-framework'); ?>
      </span></label></th>
    <td><input type="text" name="team_email" id="team_email" value="<?php echo $team_email; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>
      <tr>
    <th><label for="team_info"><strong>
        <?php _e('Info','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Input additional info.','mi-framework'); ?>
      </span></label></th>
    <td><input type="text" name="team_info" id="team_info" value="<?php echo $team_info; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>
</table>
<?php
	}
add_action( 'save_post', 'team_meta_box_save' );

	function team_meta_box_save( $post_id )
	{

	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		if( !isset( $_POST['meta_box_nonce_slide'] ) || !wp_verify_nonce( $_POST['meta_box_nonce_slide'], 'team_meta_box_nonce' ) ) return;

		if( !current_user_can( 'edit_post' ) ) return;

			if( isset( $_POST['team_position'] ) )
			update_post_meta( $post_id, 'team_position', $_POST['team_position'] );
        if( isset( $_POST['team_email'] ) )
			update_post_meta( $post_id, 'team_email', $_POST['team_email']  );
         if( isset( $_POST['team_info'] ) )
			update_post_meta( $post_id, 'team_info', $_POST['team_info']  );

}


/*-----------------------------------------------------------------------------------*/
/*	Add Metabox to Team Social Network Data
/*-----------------------------------------------------------------------------------*/
	add_action( 'add_meta_boxes', 'team_meta_social_add' );

	function team_meta_social_add()
	{
		add_meta_box( 'slide-meta-boxs', __('Social Network', 'mi-framework'), 'team_meta_social', 'mi_team', 'normal', 'high' );
	}

	function team_meta_social( $post )
	{
		$values = get_post_custom( $post->ID );
        $social_network_title = isset( $values['social_network_title'] ) ? esc_attr( $values['social_network_title'][0] ) : '';
        $pricingDetails = get_post_meta($post->ID,'pricingDetails',true);
		wp_nonce_field( 'team_meta_social_nonce', 'meta_box_nonce_social' );
		?>
<table style="width:100%;" class="form-table">
	<tr>
		<td colspan="2"><p style="padding:10px 0 0 0;">Your Social Networks.<br><em>In icon field you need to specify the icon name that can be copied from the <a target="_blank" href="//fortawesome.github.io/Font-Awesome/3.2.1/icons/#brand">website</a>. E.g. "icon-facebook".</em></p></p>
			<hr></td>
	</tr>
  <tr>
    <th style="width:25%"><label for="social_network_title"><strong>
        <?php _e('Social Networks Title','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Specify the heading for the social networks block.','mi-framework'); ?>
      </span></label>
        </th>
    <td><input type="text" name="social_network_title" id="social_network_title" style="width:70%; margin-right:4%;" value="<?php echo $social_network_title; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>

</table>

<table class="form-table" id="social_network">
	<tbody>

			 <?php

    //Obtaining the linked pricingdetails meta values
    #$pricingDetails = get_post_meta($post->ID,'pricingDetails',true);
    $c = 0;
    if ( count( $pricingDetails ) > 0 && is_array($pricingDetails)) {
        foreach( $pricingDetails as $pricingDetail ) {
            if ( isset( $pricingDetail['icon'] ) || isset( $pricingDetail['name'] ) ) {
                printf( '<p>Icon <input type="text" name="pricingDetails[%1$s][icon]" value="%2$s"  style="width:130px; margin-right:12px;"/>  List :<input type="text" name="pricingDetails[%1$s][name]" value="%3$s" style="width:394px; margin-right:12px;" />
                	<a href="#" class="remove-package">%4$s</a></p>', $c, $pricingDetail['icon'], $pricingDetail['name'], 'Remove' );
                $c = $c +1;
            }
        }
    }

    ?>
		<tr id="tr_add_network" style="border-top:1px solid #eeeeee; width:100%;">
			<th style="width:15%"><a id="add_network" class="button" href="#">Add Social Network</a></th>
			<th style="width:20%"></th>
			<th style="width:60%"></th>
			<th style="width:5%"></th>
		</tr>
		<tr style="border-top:1px solid #eeeeee;" id="titles_social_network"><th style="width:15%"><strong>Icon</strong></th><th style="width:20%"><strong>Title</strong></th><th style="width:65%"><strong>Page URL</strong></th><th style="width:5%"></th></tr>
		<script>
    var $ =jQuery.noConflict();
    $(document).ready(function() {
        var count = <?php echo $c; ?>;
        $("#add_network").click(function() {
            count = count + 1;
	$('#social_network').append('<tr style="border-top:1px solid #eeeeee;" id="network_['+count+']"><th style="width:15%">  <input type="text" name="pricingDetails['+count+'][icon]" value="" style="width:100%; margin-right: 20px;" /></th>  <th style="width:20%">  <input type="text" name="pricingDetails['+count+'][name]" value="" style="width:100%; margin-right:2%;"  /></th><th  style="width:60%"><input type="text" name="pricingDetails['+count+'][url]" value="" style="width:100%; margin-right:2%;"  /></th> <th  style="width:5%"><a href="javascript:void(0)" class="remove-package button"><?php echo "Remove"; ?></a></th></tr>' );
            return false;
        });
       $(document.body).on('click','.remove-package',function() {
            $(this).parent().remove();
        });
    });
    </script>
	</tbody>
</table>
<?php
	}
add_action( 'save_post', 'team_meta_social_save' );

	function team_meta_social_save( $post_id )
	{

	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		if( !isset( $_POST['meta_box_nonce_social'] ) || !wp_verify_nonce( $_POST['meta_box_nonce_social'], 'team_meta_social_nonce' ) ) return;

		if( !current_user_can( 'edit_post' ) ) return;
         if( isset( $_POST['social_network_title'] ) )
			update_post_meta( $post_id, 'social_network_title', $_POST['social_network_title']  );

		if( isset( $_POST['pricingDetails'] ) )
			update_post_meta( $post_id, 'pricingDetails', $_POST['pricingDetails']  );

}


/* Label */
add_filter( 'manage_mi_team_posts_columns', 'mi_set_team_columns' );
add_action( 'manage_mi_team_posts_custom_column', 'mi_team_custom_column', 10, 2 );

// Set Custom Columns
function mi_set_team_columns( $columns){
  var_dump($columns);
#unset($columns['date']);
$newColumns = array();
$newColumns['title'] = 'Name';
$newColumns['shortcode'] = 'Shortcode';
$newColumns['category'] = 'Category';
$newColumns['date'] = 'Date';
return $newColumns;
}

//  Create Custom Columns

function mi_team_custom_column( $column, $post_id ){
  
  switch( $column ){
    
    case 'shortcode' :
    // shortcode column
    $cat = get_the_term_list($post->ID,'mi_team_category');
    $cat = strip_tags( $cat );
    echo "<pre>[mi-team category='".$cat."']</pre>";
      break;
      
      case 'category' :
      // category column
      echo get_the_term_list($post->ID,'mi_team_category');
      break;
  }
  
}