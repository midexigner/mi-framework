<?php

/*-----------------------------------------------------------------------------------*/
/*	Add Metabox to Slider
/*-----------------------------------------------------------------------------------*/
	add_action( 'add_meta_boxes', 'slide_meta_box_add' );

	function slide_meta_box_add()
	{
		add_meta_box( 'slide-meta-box', __('Slider Options', 'mi-framework'), 'slide_meta_box', 'mi_slides', 'normal', 'high' );
	}

	function slide_meta_box( $post )
	{
		$values = get_post_custom( $post->ID );
		$caption = isset( $values['caption'] ) ? esc_attr( $values['caption'][0] ) : '';
		$sub_title = isset( $values['sub_title'] ) ? esc_attr( $values['sub_title'][0] ) : '';
		$url = isset( $values['url'] ) ? esc_attr( $values['url'][0] ) : '';
    $styling = isset( $values['styling'] ) ? esc_attr( $values['styling'][0] ) : '';
		$button_text = isset( $values['button_text'] ) ? esc_attr( $values['button_text'][0] ) : '';
		wp_nonce_field( 'slide_meta_box_nonce', 'meta_box_nonce_slide' );
		?>
<table style="width:100%;" class="form-table">
	<tr>
		<td colspan="2">
            <p>Please fill additional fields for slide.</p>
			<hr>
    </td>
	</tr>
	 <tr>
      <th style="width:25%"><label for="styling"><strong><?php _e('Styling','mi-framework');?></strong>
				<span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
					<?php _e('Example:Caption for slide right, center or left.','mi-framework'); ?></span></label></th>
    <td>
     <select name="styling" id="styling" style="width:70%; margin-right:4%;">
         <option value="left" <?php if($styling=='left'){echo 'selected';} ?>>Left</option>
         <option value="center" <?php if($styling=='center'){echo 'selected';} ?>>Center</option>
         <option value="right" <?php if($styling=='right'){echo 'selected';} ?>>Right</option>
     </select>

      </td>
  </tr>
	<tr>
      <th style="width:25%"><label for="sub_title"><strong><?php _e('Sub Title','mi-framework');?></strong>
				<span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
					<?php _e('Example:Input your sub title for slide.','mi-framework'); ?></span></label></th>
    <td> <input type="text" name="sub_title" id="sub_title" style="width:70%; margin-right:4%;" value="<?php echo trim($sub_title); ?>">
      </td>
  </tr>
  <tr>
      <th style="width:25%"><label for="caption"><strong><?php _e('Caption','mi-framework');?></strong>
				<span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
					<?php _e('Example:Input your caption for slide (HTML tags are allowed).','mi-framework'); ?></span></label></th>
    <td> <textarea name="caption" id="caption" style="width:70%; margin-right:4%;" rows="5" ><?php echo trim($caption); ?></textarea>
      </td>
  </tr>
  <tr>
       <th style="width:25%"><label for="url"><strong><?php _e('URL','mi-framework');?></strong>
				 <span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
					 <?php _e('Example:Input the slide URL (can be external).','mi-framework'); ?></span></label></th>
    <td><input type="text" name="url" id="url" value="<?php echo $url; ?>" style="width:70%; margin-right:4%;" />
     </td>
  </tr>
	<tr>
			 <th style="width:25%"><label for="button_text"><strong><?php _e('Button Text','mi-framework');?></strong>
				 <span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
					 <?php _e('Example:Set the slide Button text.','mi-framework'); ?></span></label></th>
		<td><input type="text" name="button_text" id="button_text" value="<?php echo $button_text; ?>" style="width:70%; margin-right:4%;" />
		 </td>
	</tr>
</table>
<?php
	}
add_action( 'save_post', 'slide_meta_box_save' );

	function slide_meta_box_save( $post_id )
	{

	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		if( !isset( $_POST['meta_box_nonce_slide'] ) || !wp_verify_nonce( $_POST['meta_box_nonce_slide'], 'slide_meta_box_nonce' ) ) return;

		if( !current_user_can( 'edit_post' ) ) return;

			if( isset( $_POST['caption'] ) )
			update_post_meta( $post_id, 'caption', $_POST['caption'] );
		if( isset( $_POST['sub_title'] ) )
			update_post_meta( $post_id, 'sub_title', $_POST['sub_title']  );
			if( isset( $_POST['url'] ) )
				update_post_meta( $post_id, 'url', $_POST['url']  );
				if( isset( $_POST['button_text'] ) )
					update_post_meta( $post_id, 'button_text', $_POST['button_text']  );
        if( isset( $_POST['styling'] ) )
			update_post_meta( $post_id, 'styling', $_POST['styling']  );

}


/*-----------------------------------------------------------------------------------*/
/*	Add Metabox to Slider Title Setting
/*-----------------------------------------------------------------------------------*/

add_action( 'add_meta_boxes', 'slide_meta_box_add_title' );

function slide_meta_box_add_title()
{
	add_meta_box( 'slide-meta-box-settings', __('MI Slide Title', 'mi-framework'), 'slide_meta_box_title', 'mi_slides', 'normal', 'high' );
}
function slide_meta_box_title( $post )
{
	$values = get_post_custom( $post->ID );
	#$caption = isset( $values['caption'] ) ? esc_attr( $values['caption'][0] ) : '';
	#$sub_title = isset( $values['sub_title'] ) ? esc_attr( $values['sub_title'][0] ) : '';
	#$url = isset( $values['url'] ) ? esc_attr( $values['url'][0] ) : '';
	#$styling = isset( $values['styling'] ) ? esc_attr( $values['styling'][0] ) : '';
	wp_nonce_field( 'slide_meta_box_nonce_title', 'meta_box_nonce_slide_title' );
?>
<table style="width:100%;" class="form-table">
<tr>

<th style="width:25%"><label for="font_color"><strong><?php _e('Font Color','mi-framework');?></strong>
</label></th>
<td>
	<input type='text' name="font_color" value="" style="width:70%; margin-right:4%;" value="<?php //echo trim($sub_title); ?>" />
</td>
<th style="width:25%"><label for="font_size"><strong><?php _e('Font size (px)','mi-framework');?></strong>
	</label></th>
<td> 	<input type='text' name="font_size" value="" style="width:70%; margin-right:4%;" value="<?php //echo trim($sub_title); ?>" />
</td>

</tr>
<tr>
	<th style="width:25%"><label for="line_height"><strong><?php _e('Line Height (px)','mi-framework');?></strong>
		</label></th>
<td> 	<input type='text' name="line_height" value="" style="width:70%; margin-right:4%;" value="<?php //echo trim($sub_title); ?>" />
	</td>
	<th style="width:25%"><label for="letter_spacing"><strong><?php _e('Letter Spacing (px)','mi-framework');?></strong>
		</label></th>
<td> 	<input type='text' name="letter_spacing" value="" style="width:70%; margin-right:4%;" value="<?php //echo trim($sub_title); ?>" />
	</td>
</tr>
<tr>
	<td colspan="4">
					<h5>Title Style</h5>
		<hr>
	</td>
</tr>
</table>
<?php
	}
add_action( 'save_post', 'slide_meta_box_save_title' );

	function slide_meta_box_save_title( $post_id )
	{

	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		if( !isset( $_POST['meta_box_nonce_slide_title'] ) || !wp_verify_nonce( $_POST['meta_box_nonce_slide_title'], 'slide_meta_box_nonce_title' ) ) return;

		if( !current_user_can( 'edit_post' ) ) return;

			if( isset( $_POST['caption'] ) )
			update_post_meta( $post_id, 'caption', $_POST['caption'] );
		if( isset( $_POST['sub_title'] ) )
			update_post_meta( $post_id, 'sub_title', $_POST['sub_title']  );
			if( isset( $_POST['url'] ) )
				update_post_meta( $post_id, 'url', $_POST['url']  );
        if( isset( $_POST['styling'] ) )
			update_post_meta( $post_id, 'styling', $_POST['styling']  );

}



/* Label */
add_filter( 'manage_mi_slides_posts_columns', 'mi_set_slide_columns' );
add_action( 'manage_mi_slides_posts_custom_column', 'mi_slide_custom_column', 10, 2 );

// Set Custom Columns
function mi_set_slide_columns( $columns){
	var_dump($columns);
#unset($columns['date']);
$newColumns = array();
$newColumns['title'] = 'Slide Title';
$newColumns['shortcode'] = 'Shortcode';
$newColumns['category'] = 'Category';
$newColumns['date'] = 'Date';
return $newColumns;
}

//  Create Custom Columns

function mi_slide_custom_column( $column, $post_id ){
	
	switch( $column ){
		
		case 'shortcode' :
		// shortcode column
		$cat = get_the_term_list($post->ID,'sliders');
		$cat = strip_tags( $cat );
		echo "<pre>[mi-slide category='".$cat."']</pre>";
			break;
			
			case 'category' :
			// category column
			echo get_the_term_list($post->ID,'sliders');
			break;
	}
	
}