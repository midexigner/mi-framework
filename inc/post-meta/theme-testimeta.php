<?php

/*-----------------------------------------------------------------------------------*/
/*	Add Metabox to Testimonials
/*-----------------------------------------------------------------------------------*/
	add_action( 'add_meta_boxes', 'testimonials_meta_box_add' );

	function testimonials_meta_box_add()
	{
		add_meta_box( 'slide-meta-box', __('Testimonials Options', 'mi-framework'), 'testimonials_meta_box', 'mi_testimonials', 'normal', 'high' );
	}

	function testimonials_meta_box( $post )
	{
		$values = get_post_custom( $post->ID );
		$testi_caption = isset( $values['testi_caption'] ) ? esc_attr( $values['testi_caption'][0] ) : '';
		$testi_url = isset( $values['testi_url'] ) ? esc_attr( $values['testi_url'][0] ) : '';
        $testi_email = isset( $values['testi_email'] ) ? esc_attr( $values['testi_email'][0] ) : '';
        $testi_info = isset( $values['testi_info'] ) ? esc_attr( $values['testi_info'][0] ) : '';
		wp_nonce_field( 'testimonials_meta_box_nonce', 'meta_box_nonce_slide' );
		?>
<table style="width:100%;" class="form-table">
	<tr>
		<td colspan="2"><p>Please fill additional fields for testimonial.</p>
			<hr></td>
	</tr>
  <tr>
    <th style="width:25%"><label for="testi_caption"><strong>
        <?php _e('Name','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Input author\'s name.','mi-framework'); ?>
      </span></label>
        </th>
    <td><input type="text" name="testi_caption" id="testi_caption" style="width:70%; margin-right:4%;" value="<?php echo $testi_caption; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>
  <tr>
    <th><label for="testi_url"><strong>
        <?php _e('URL','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Input author\'s URL.','mi-framework'); ?>
      </span></label></th>
    <td><input type="text" name="testi_url" id="testi_url" value="<?php echo $testi_url; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>
     <tr>
    <th><label for="testi_email"><strong>
        <?php _e('Email','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Input author\'s email address.','mi-framework'); ?>
      </span></label></th>
    <td><input type="text" name="testi_email" id="testi_email" value="<?php echo $testi_email; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>
      <tr>
    <th><label for="testi_info"><strong>
        <?php _e('Info','mi-framework');?>
        </strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">
      <?php _e('Input author\'s additional info.','mi-framework'); ?>
      </span></label></th>
    <td><input type="text" name="testi_info" id="testi_info" value="<?php echo $testi_info; ?>" style="width:70%; margin-right:4%;" />
      </td>
  </tr>
</table>
<?php
	}
add_action( 'save_post', 'testimonials_meta_box_save' );

	function testimonials_meta_box_save( $post_id )
	{

	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		if( !isset( $_POST['meta_box_nonce_slide'] ) || !wp_verify_nonce( $_POST['meta_box_nonce_slide'], 'testimonials_meta_box_nonce' ) ) return;

		if( !current_user_can( 'edit_post' ) ) return;

			if( isset( $_POST['testi_caption'] ) )
			update_post_meta( $post_id, 'testi_caption', $_POST['testi_caption'] );
		if( isset( $_POST['testi_url'] ) )
			update_post_meta( $post_id, 'testi_url', $_POST['testi_url']  );
        if( isset( $_POST['testi_email'] ) )
			update_post_meta( $post_id, 'testi_email', $_POST['testi_email']  );
         if( isset( $_POST['testi_info'] ) )
			update_post_meta( $post_id, 'testi_info', $_POST['testi_info']  );

}
