<?php 

// create shortcode to slider

//add_shortcode( 'mi-slide', 'mi_slider' );
function mi_slider( $atts ) {
    ob_start();
    // define attributes and their defaults
    global $post;
    extract( shortcode_atts( array (
        'order' => 'date',
        'orderby' => 'title',
        'posts' => -1,
        'category' => '',
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => 'mi_slide',
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => $posts,
        'category_name' => $category,
    );
    $slide_loop = new WP_Query( $options );
   $slide_indicators = new WP_Query( $options ); ?>
     
     <div id="bootstrap-touch-slider" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="5000">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  <?php  while ( $slide_indicators->have_posts() ) : $slide_indicators->the_post(); ?>
    <li data-target="#bootstrap-touch-slider" data-slide-to="<?php echo $slide_indicators->current_post; ?>" <?php if( $slide_indicators->current_post == 0 ):?>class="active"<?php endif; ?>></li>
    <?php endwhile;
      /* Restore original Post Data */
      wp_reset_postdata();  ?> 
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <?php
    $i=0;
         while ( $slide_loop->have_posts() ) : $slide_loop->the_post();
$image_id = get_post_thumbnail_id();
$featured_image = wp_get_attachment_image_src( $image_id,'slider-img' );
$i++;
         ?>
         <div class="item <?php if( $slide_loop->current_post == 0 ):?>active<?php endif; ?>">
      <img src="<?php echo $featured_image[0]; ?>" alt="<?php the_title(); ?>"  class="slide-image">
      <div class="bs-slider-overlay"></div>

                    <div class="container">
                        <div class="row">
                            <!-- Slide Text Layer -->
                            <?php if(get_post_meta($post->ID,'styling',true)=='left'){ ?>
                            <div class="slide-text slide_style_left">
                 <h1 data-animation="animated zoomInRight"><?php the_title(); ?></h1>
                 <p data-animation="animated fadeInLeft"><?php echo get_post_meta($post->ID,'caption',true); ?></p>
                                <a href="<?php echo get_post_meta($post->ID,'url',true); ?>" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft"><?php echo of_get_option('slide_button'); ?></a>
                                
                            </div>
                            <?php }else if(get_post_meta($post->ID,'styling',true)=='center'){ ?>
                            
                            <div class="slide-text slide_style_center">
                        <h1 data-animation="animated flipInX"><?php the_title(); ?></h1>
                        <p data-animation="animated lightSpeedIn"><?php echo get_post_meta($post->ID,'caption',true); ?></p>
                        <a href="<?php echo get_post_meta($post->ID,'url',true); ?>" target="_blank"  class="btn btn-primary" data-animation="animated fadeInDown"><?php echo of_get_option('slide_button'); ?></a>
                    </div>
                      
                        <?php }else  if(get_post_meta($post->ID,'styling',true)=='right'){?>
                        <div class="slide-text slide_style_right">
                        <h1 data-animation="animated zoomInLeft"><?php the_title(); ?></h1>
                        <p data-animation="animated fadeInRight"><?php echo get_post_meta($post->ID,'caption',true); ?></p>
                        <a href="<?php echo get_post_meta($post->ID,'url',true); ?>" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft"><?php echo of_get_option('slide_button'); ?></a>
                       
                    </div>
                        <?php } ?>
                        </div>
                    </div>
    </div>
    <?php endwhile;
      /* Restore original Post Data */
      wp_reset_postdata();  ?> 
</div>
  <!-- Controls -->
  <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    
    
<?php
        $myvariable = ob_get_clean();
        return $myvariable;
    //}
}    




 ?>