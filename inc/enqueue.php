<?php 


if ( ! function_exists( 'mi_scripts' ) ) {
	function mi_scripts() {
		// Adds support for pages with threaded comments
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		wp_enqueue_style( 'plugin', THEMEROOT . '/css/plugin.css', array(), '2.0.0', 'all' );
		wp_enqueue_style( 'default', THEMEROOT . '/css/default.css', array(), '2.0.0', 'all' );
		#wp_enqueue_style( 'raleway', 'https://fonts.googleapis.com/css?family=Raleway:200,300,500' );
		wp_enqueue_style( 'style', THEMEROOT . '/css/style.css', array(), '2.0.0', 'all' );
		wp_enqueue_script("jquery");
		wp_register_script( 'plugins' , SCRIPTS . 'plugins.js', false, '2.0.0', true );
		wp_enqueue_script( 'plugins' );
		wp_enqueue_script( 'mi', SCRIPTS . 'mi.js', false, '2.0.0', true );
		wp_enqueue_script( 'main', SCRIPTS . 'main.js', false, '2.0.0', true );
        wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700', false );

	}

	add_action( 'wp_enqueue_scripts', 'mi_scripts' );
}
 ?>