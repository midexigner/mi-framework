<?php
/**
 * init.php
 *
 * Load the widget file
 * Package mi-framework Theme
 * Since 1.0
 * Author MI Dexigner : http://www.midexigner.com
 * Copyright (c) 2018, MI Dexigner (TM)
 * Link http://www.midexigner.com
 */
 ?>

<?php
if ( ! file_exists( FRAMEWORK . '/wp-bootstrap-navwalker.php' ) ) {
	// file does not exist... return an error.
	return new WP_Error( 'wp-bootstrap-navwalker-missing', __( 'It appears the wp-bootstrap-navwalker.php file may be missing.', 'mi-framework' ) );
} else {
	// file exists... require it.
    include FRAMEWORK .'wp-bootstrap-navwalker.php';
}
 include FRAMEWORK .'walkernav.php';
/* register-plugins */
 include FRAMEWORK .'register-plugins.php';

/* post type */
include FRAMEWORK .'/post-type/slide.php';
include FRAMEWORK .'post-type/testimonials.php';
include FRAMEWORK .'post-type/portfoios.php';
include FRAMEWORK .'post-type/masonry_gallerys.php';
include FRAMEWORK .'post-type/team.php';
include FRAMEWORK .'post-type/faqs.php';

/* post meta */
include FRAMEWORK .'post-meta/theme-slidermeta.php';
include FRAMEWORK .'post-meta/theme-testimeta.php';
include FRAMEWORK .'post-meta/theme-team.php';

/* shortcode */
#include FRAMEWORK .'shortcode.php';
#include FRAMEWORK,'generate-styling.php');
include FRAMEWORK .'importer.php';
include FRAMEWORK .'widget.php';
include FRAMEWORK .'functions.php';
if(file_exists('kingcomposer.php')){
include FRAMEWORK .'kingcomposer.php';
}
include get_template_directory() . '/admin/admin-init.php';
?>
