<?php

/* MI Panel */

//add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function = '', $icon_url = '', $position = null )
function mi_admin_page() {
	
	//Generate Theme Admin Page
	add_menu_page( 'MI Panel ', 'MI Panel', 'manage_options', 'mi_panel', 'mi_welcome_create_page',get_template_directory_uri() . '/images/icons/mi-framework-icon.png', 3 );

add_submenu_page( 'mi_panel', 'MI Panel', 'Welcome', 'manage_options', 'mi_panel', 'mi_welcome_create_page' );
add_submenu_page( 'mi_panel', 'MI Panel', 'System Status', 'manage_options', 'mi_panel_status', 'mi_system_status_create_page' );
	
}
add_action( 'admin_menu', 'mi_admin_page');

function mi_welcome_create_page(){
	echo '<h2>Welcome to MI Framework</h2>
	<p>
						Thank you for choosing MI Framework - Creative Multi-Purpose WordPress Theme, the number 1 bestselling creative theme on the market. Start building your amazing website now, and you’ll find out why so many people trust our product.  You just installed a one-way ticket to online perfection.						</p>
';
}

function mi_system_status_create_page(){
	echo '<h2>Welcome to system_status</h2>
	<p>
						Thank you for choosing MI Framework - Creative Multi-Purpose WordPress Theme, the number 1 bestselling creative theme on the market. Start building your amazing website now, and you’ll find out why so many people trust our product.  You just installed a one-way ticket to online perfection.						</p>
';
}




function footer_bottom_text() {
	do_action('footer_bottom_text');
}
function copyright() {
	echo ' <p>&copy; '. date('Y').' Designed &amp; Developed by <a href="'.site_url().'" target="_blank" rel="nofollow" >MI Dexigner</a>  |   Powered by <a href="//wordpress.org" rel="nofollow">WordPress</a></p>';
}
add_action('footer_bottom_text', 'copyright', 7);
?>
