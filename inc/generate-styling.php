<?php
/**
 * ----------------------------------------------------------------------------------------
 * 1.0 - Include the generated CSS in the page header.
 * ----------------------------------------------------------------------------------------
 */
if ( ! function_exists( 'mi_load_wp_head' ) ) {
	function mi_load_wp_head() {?>
<meta  name="author" content="MI Dexigner" />
<!-- favicon and Apple Icons -->
<link rel="shortcut icon" href="<?php echo of_get_option( 'favicon_path'); ?>" type="image/x-ico" >

<!-- generated CSS -->
		<style type="text/css">


		</style>

	<script>
//<![CDATA[
function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }
loadCSS("//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css");
//]]>
//<![CDATA[
function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }
loadCSS("//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css");
//]]>
</script>


<!-- [if it IE 9]
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->


<?php
}add_action( 'wp_head', 'mi_load_wp_head' );
}

?>
