<?php

class mi_Widget_Social_Icon extends WP_Widget {

/**
	 * Specifies the widget name, description, class name and instatiates it
	 */
	public function __construct() {
		parent::__construct(
			'widget-social-icon',
			__( 'MI: Social Icon ', 'mi-framework' ),
			array(
				'classname'   => 'widget-social-icon',
				'description' => __( 'A  widget that displays Icons.', 'mi-framework' )
			)
		);
	}


	/**
	 * Generates the back-end layout for the widget
	 */
	public function form( $instance ) {
		$fa_icons_array = array(
		            'fa-500px'              =>  '500px',
		            "fa-adn"                => "ADN",
		            'fa-amazon'             => 'Amazon',
		            "fa-android"            => "Android",
		            "fa-apple"              => "Apple",
		            "fa-behance"            => "Behance",
		            "fa-behance-square"     => "Behance Square",
		            "fa-bitbucket"          => "Bitbucket",
		            "fa-bitbucket-square"   => "Bitbucket-Square",
		            "fa-bitcoin"            => "Bitcoin",
		            "fa-btc"                => "BTC",
		            "fa-css3"               => "CSS3",
		            "fa-dribbble"           => "Dribbble",
		            "fa-dropbox"            => "Dropbox",
		            "fa-facebook"           => "Facebook",
		            "fa-facebook-f"         => "Facebook-F",
		            "fa-facebook-square"    => "Facebook-Square",
		            "fa-facebook-official"  => "Facebook-Official",
		            "fa-flickr"             => "Flickr",
		            "fa-foursquare"         => "Foursquare",
		            "fa-github"             => "GitHub",
		            "fa-github-alt"         => "GitHub-Alt",
		            "fa-github-square"      => "GitHub-Square",
		            "fa-gittip"             => "Gittip",
		            "fa-google-plus"        => "Google Plus",
		            "fa-google-plus-circle" => "Google Plus-Circle",
		            "fa-google-plus-square" => "Google Plus-Square",
		            "fa-google-plus-official" => "Google Plus-Official",
		            "fa-html5"              => "HTML5",
		            "fa-instagram"          => "Instagram",
		            "fa-linkedin"           => "LinkedIn",
		            "fa-linkedin-square"    => "LinkedIn-Square",
		            "fa-linux"              => "Linux",
		            "fa-envelope"           => "Mail",
		            "fa-envelope-o"         => "Mail Alt",
		            "fa-envelope-square"    => "Mail Square",
		            "fa-maxcdn"             => "MaxCDN",
		            "fa-paypal"             => "Paypal",
		            "fa-pinterest"          => "Pinterest",
		            "fa-pinterest-p"        => "Pinterest-P",
		            "fa-pinterest-square"   => "Pinterest-Square",
		            "fa-renren"             => "Renren",
		            "fa-skype"              => "Skype",
		            "fa-stackexchange"      => "StackExchange",
		            'fa-tripadvisor'        => 'Trip Advisor',
		            "fa-trello"             => "Trello",
		            "fa-tumblr"             => "Tumblr",
		            "fa-tumblr-square"      => "Tumblr-Square",
		            "fa-twitter"            => "Twitter",
		            "fa-twitter-square"     => "Twitter-Square",
		            'fa-vimeo'              => 'Vimeo',
		            'fa-vimeo-square'       => 'Vimeo Square',
		            'fa-vine'               => 'Vine',
		            "fa-vk"                 => "VK",
		            "fa-weibo"              => "Weibo",
		            'fa-wikipedia-w'        => 'Wikipedia',
		            "fa-windows"            => "Windows",
		            'fa-wordpress'          => 'Wordpress',
		            "fa-xing"               => "Xing",
		            "fa-xing-square"        => "Xing-Square",
		            "fa-youtube"            => "YouTube",
		            "fa-youtube-play"       => "YouTube Play",
		            "fa-youtube-square"     => "YouTube-Square",
								"fa-rss"							=> 'RSS',
								"fa-rss-square"    			 => "RSS-Square",
		        );
		// Default widget settings
		$defaults = array(
			'type'   => 'normal',
			'icon_pack'   => 'FontAwesome',
			'icon_listing'=> '',
			'custom_size' => '',
			'custom_shape'      => '',
			'link' =>'#',
			'target'=>'new_window',
			'color'=>'',
			'color_hover'=>'',
			'background_color'=>'',
			'background_color_hover'=>'',
			'border_width'=>'',
			'border_radius'=>'',
			'border_color'=>'',
			'border_color_hover'=>'',
			'margin'=>'',
			'padding'=>''
		);

		$instance = wp_parse_args( (array) $instance, $defaults );

		// The widget content ?>
		<!-- Type -->
		<p>
			<label for="<?php echo $this->get_field_id( 'type' ); ?>"><?php _e( 'Type:', 'mi-framework' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'type' ); ?>" id="<?php echo $this->get_field_id( 'type' ); ?>" class="widefat">
				<option value="" >Select Type</option>
				<option value="normal" <?php if($instance['type']=='normal'){echo 'selected';} ?>>Normal</option>
				<option value="circle" <?php if($instance['type']=='circle'){echo 'selected';} ?>>Circle</option>
				<option value="square" <?php if($instance['type']=='square'){echo 'selected';} ?>>Square</option>
			</select>
		</p>

		<!-- icon pack -->
		<p>
			<label for="<?php echo $this->get_field_id( 'icon_pack' ); ?>"><?php _e( 'Icon Pack:', 'mi-framework' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'icon_pack' ); ?>" id="<?php echo $this->get_field_id( 'icon_pack' ); ?>" class="widefat">
				<option value="" >Select Icon</option>
					<option value="FontAwesome" <?php if($instance['icon_pack']=='FontAwesome'){echo 'selected';} ?>>FontAwesome</option>
				</select>
		</p>
		<!-- icon listing -->
		<p>
			<label for="<?php echo $this->get_field_id( 'icon_listing' ); ?>"><?php _e( 'Font Awesome Icon:', 'mi-framework' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'icon_listing' ); ?>" id="<?php echo $this->get_field_id( 'icon_listing' ); ?>" class="widefat">
				<option value="">Select Icon</option>
				<?php foreach (	$fa_icons_array as $fkey => $fvalue) { ?>
				<option value="<?php echo $fkey; ?>" <?php if($instance['icon_listing']==$fkey){echo 'selected';} ?>><?php echo $fvalue; ?></option>
					<?php 	} ?>
			</select>
		</p>
		<!-- custom size -->
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_size' ); ?>"><?php _e( 'Custom size (px)', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'custom_size' ); ?>" name="<?php echo $this->get_field_name( 'custom_size' ); ?>" value="<?php echo esc_attr( $instance['custom_size'] ); ?>">

		</p>
		<!-- custom shape -->
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_shape' ); ?>"><?php _e( 'Custom Shape size (px) - (only For Circle and Square Type):', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'custom_shape' ); ?>" name="<?php echo $this->get_field_name( 'custom_shape' ); ?>" value="<?php echo esc_attr( $instance['custom_shape'] ); ?>">

		</p>
		<!-- link -->
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Link:', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo esc_attr( $instance['link'] ); ?>">

		</p>
		<!-- Target -->
		<p>
			<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e( 'Target:', 'mi-framework' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'target' ); ?>" id="<?php echo $this->get_field_id( 'target' ); ?>" class="widefat">
				<option value="" >Select Target</option>
				<option value="same_window" <?php if($instance['target']=='same_window'){echo 'selected';} ?>>Same Window</option>
				<option value="new_window" <?php if($instance['target']=='new_window'){echo 'selected';} ?>>New Window</option>
			</select>
				</p>
		<!-- Color -->
		<p>
			<label for="<?php echo $this->get_field_id( 'color' ); ?>"><?php _e( 'Color:', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'color' ); ?>" name="<?php echo $this->get_field_name( 'color' ); ?>" value="<?php echo esc_attr( $instance['color'] ); ?>">

		</p>
		<!-- Hover Color -->
		<p>
			<label for="<?php echo $this->get_field_id( 'color_hover' ); ?>"><?php _e( 'Hover Color', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'color_hover' ); ?>" name="<?php echo $this->get_field_name( 'color_hover' ); ?>" value="<?php echo esc_attr( $instance['color_hover'] ); ?>">

		</p>
		<!-- Background Color -->
		<p>
			<label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background Color (Only For Circle and Square Type)', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo esc_attr( $instance['background_color'] ); ?>">
		</p>
		<!-- Background Hover Color  -->
		<p>
			<label for="<?php echo $this->get_field_id( 'background_color_hover' ); ?>"><?php _e( 'Background Hover Color (Only For Circle and Square Type):', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'background_color_hover' ); ?>" name="<?php echo $this->get_field_name( 'background_color_hover' ); ?>" value="<?php echo esc_attr( $instance['background_color_hover'] ); ?>">

		</p>
		<!-- border width -->
		<p>
			<label for="<?php echo $this->get_field_id( 'border_width' ); ?>"><?php _e( 'Border Width (px) - (Only For Circle and Square Type):', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'border_width' ); ?>" name="<?php echo $this->get_field_name( 'border_width' ); ?>" value="<?php echo esc_attr( $instance['border_width'] ); ?>">

		</p>
		<!-- border radius -->
		<p>
			<label for="<?php echo $this->get_field_id( 'border_radius' ); ?>"><?php _e( 'Border Radius (px) - (Only For Circle and Square Type)', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'border_radius' ); ?>" name="<?php echo $this->get_field_name( 'border_radius' ); ?>" value="<?php echo esc_attr( $instance['border_radius'] ); ?>">

		</p>
		<!-- border color -->
		<p>
			<label for="<?php echo $this->get_field_id( 'border_color' ); ?>"><?php _e( 'Border Color (Only For Circle and Square Type):', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'border_color' ); ?>" name="<?php echo $this->get_field_name( 'border_color' ); ?>" value="<?php echo esc_attr( $instance['border_color'] ); ?>">

		</p>
		<!-- border color hover -->
		<p>
			<label for="<?php echo $this->get_field_id( 'border_color_hover' ); ?>"><?php _e( 'Border Hover Color (Only For Circle and Square Type):', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'border_color_hover' ); ?>" name="<?php echo $this->get_field_name( 'border_color_hover' ); ?>" value="<?php echo esc_attr( $instance['border_color_hover'] ); ?>">

		</p>
		<!-- margin -->
		<p>
			<label for="<?php echo $this->get_field_id( 'margin' ); ?>"><?php _e( 'Margin (top right bottom left):', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'margin' ); ?>" name="<?php echo $this->get_field_name( 'margin' ); ?>" value="<?php echo esc_attr( $instance['margin'] ); ?>">

		</p>
		<!-- padding -->
		<p>
			<label for="<?php echo $this->get_field_id( 'padding' ); ?>"><?php _e( 'Padding (top right bottom left):', 'mi-framework' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'padding' ); ?>" name="<?php echo $this->get_field_name( 'padding' ); ?>" value="<?php echo esc_attr( $instance['padding'] ); ?>">

		</p>


	 <?php
	}

	/**
	 * Processes the widget's values
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Update values
		$instance['type']  												= strip_tags( stripslashes( $new_instance['type'] ) );
		$instance['icon_pack']  									= strip_tags( stripslashes( $new_instance['icon_pack'] ) );
		$instance['icon_listing'] 								= strip_tags( stripslashes( $new_instance['icon_listing'] ) );
		$instance['custom_size']  								= strip_tags( stripslashes( $new_instance['custom_size'] ) );
		$instance['custom_shape']  								= strip_tags( stripslashes( $new_instance['custom_shape'] ) );
		$instance['link']   											= strip_tags( stripslashes( $new_instance['link'] ) );
		$instance['target'] 											= strip_tags( stripslashes( $new_instance['target'] ) );
		$instance['color']      									= strip_tags( stripslashes( $new_instance['color'] ) );
		$instance['color_hover']    							= strip_tags( stripslashes( $new_instance['color_hover'] ) );
		$instance['background_color']      				= strip_tags( stripslashes( $new_instance['background_color'] ) );
		$instance['background_color_hover']      	= strip_tags( stripslashes( $new_instance['background_color_hover'] ) );
		$instance['border_width']      						= strip_tags( stripslashes( $new_instance['border_width'] ) );
		$instance['border_radius']      					= strip_tags( stripslashes( $new_instance['border_radius'] ) );
		$instance['border_color']     						= strip_tags( stripslashes( $new_instance['border_color'] ) );
		$instance['border_color_hover']      			= strip_tags( stripslashes( $new_instance['border_color_hover'] ) );
		$instance['margin']     									= strip_tags( stripslashes( $new_instance['margin'] ) );
		$instance['padding']      								= strip_tags( stripslashes( $new_instance['padding'] ) );
		return $instance;
	}


	/**
	 * Output the contents of the widget
	 */
	public function widget( $args, $instance ) {
		// Extract the arguments
		extract( $args );
		$type  = $instance['type'];
		$icon_pack  = $instance['icon_pack'];
		$icon_listing  = $instance['icon_listing'];
		$custom_size = $instance['custom_size'];
		$custom_shape = $instance['custom_shape'];
		$link   = $instance['link'];
		$target = $instance['target'];
		$color  = $instance['color'];
		$color_hover = $instance['color_hover'];
		$background_color= $instance['background_color'];
		$background_color_hover = $instance['background_color_hover'];
		$border_width = $instance['border_width'];
		$border_radius = $instance['border_radius'];
		$border_color = $instance['border_color'];
		$border_color_hover = $instance['border_color_hover'];
		$margin = $instance['margin'];
		$padding= $instance['padding'];

		// Display the markup before the widget (as defined in functions.php)
		echo $before_widget;

	if ( $icon_listing ) :
		if($type=="circle" or $type == "square"){
		?>
		<span class="<?php echo $type; ?>"
			style="
					<?php if($custom_size){?>font-size:<?php echo $custom_size;?>;<?php }?>
					<?php if($color){?>	color:<?php echo $color;?>;<?php }?>
					<?php if($background_color){?>background-color:<?php echo $background_color;?>;<?php }?>
					<?php if($border_width){?>	border-width:<?php echo $border_width;?>;<?php }?>
					<?php if($border_radius){?>	border-radius:<?php echo $border_radius;?>;<?php }?>
					<?php if($border_color){?>border-color:<?php echo $border_color;?><?php }?>
					<?php if($padding){?>padding:<?php echo $padding;?>;<?php }?>
					<?php if($margin){?>margin:<?php echo $margin;?>;<?php }?>
			">
<?php }elseif($type=="normal"){ ?>
	<span class="<?php echo $type; ?>">
<?php }?>
		<a href="<?php echo $link; ?>" <?php  if($target =='new_window'){echo' target="_blank "';}else{echo' target="_self "'; }?> 
			style="
						<?php if($custom_size){?>	font-size:<?php echo $custom_size;?>;<?php }?>
						<?php if($color){?>	color:<?php echo $color;?>;<?php }?>
						<?php if($padding){?>padding:<?php echo $padding;?>;<?php }?>
						<?php if($margin){?>margin:<?php echo $margin;?>; <?php }?>
					"
			>
				<i class="fa <?php echo $icon_listing; ?>"></i>
			</a>
		<?php

		echo '</span>';
endif;
		// Display the markup after the widget (as defined in functions.php)?>

		
		<?php
		echo $after_widget;
	}


}

// Register the widget using an annonymous function
function register_social_icon_widgets() {
	register_widget( 'mi_Widget_Social_Icon' );
}
add_action( 'widgets_init', 'register_social_icon_widgets' );
 ?>
