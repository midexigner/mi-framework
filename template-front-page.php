<?php
/**
* template-front-page.php
*
 * Template Name: Home Page
* The template for displaying Homepage.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>
 <?php get_header();  ?>
<?php while(have_posts() ):the_post(); ?>
 <?php the_content(); ?>
<?php endwhile;  wp_reset_postdata();  ?>
<?php get_footer(); ?>
