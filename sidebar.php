<?php 
/**
* sidebar.php
*
* The primary sidebar.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
*/

 ?>

 <?php if(is_active_sidebar('sidebar')):  ?>

 	<aside class="sidebar col-md-4" role="complementary">
 	<?php dynamic_sidebar('sidebar'); ?>

 	</aside>
 	<!-- end of sidebar -->
 	<?php endif; ?>