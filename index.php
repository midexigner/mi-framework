<?php
/**
* index.php
*
* The main template file
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
*/
?>

<!--  header include -->

<?php get_header(); ?>
<!-- // being banner -->
<?php 
      
        $args_cat = array(
          'include' => '1, 9, 8'
        );
        
        $categories = get_categories($args_cat);
        $count = 0;
        $bullets = '';
        foreach($categories as $category):
          
          $args = array( 
            'type' => 'post',
            'posts_per_page' => 1,
            'category__in' => $category->term_id,
            //'category__not_in' => array( 10 ),
          );

          $lastBlog = new WP_Query( $args ); 
          
          if( $lastBlog->have_posts() ): 
            while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
<section id="post-<?php the_ID();?>" class="background-image" data-imgurl="https://placeimg.com/1400/480/any" >
      <div class="jumbotron jumbotron-fluid">
  <div class="container">
     <div class="row d-flex row d-flex justify-content-center align-items-center caption-text">
    <?php the_title( sprintf('<h1 class="entry-title display-4"><a href="%s">', esc_url( get_permalink() ) ),'</a></h1>' ); ?>
  
<small><?php the_category(' '); ?></small>
<?php echo mi_breadcrumb(); ?>
    </div>
  </div>
</div>
    </section>
    <?php endwhile;
            
          endif;
          
          wp_reset_postdata();
        
        endforeach;
      
      ?>
<!-- // end banner -->
<!-- main contain area -->
	<div class="container">
		<div class="row">

<!-- start content -->

 <div class="main-content col-md-8">
<?php  if (have_posts()) : while (have_posts()) :the_post();?>
    <?php  get_template_part('content',get_post_format()); ?>
            <?php  endwhile; ?>


            <!-- post navigation -->
	<?php mi_paging_nav() ?>

            <!-- end of post navigation -->

              <?php  else: ?>
               <?php  get_template_part('content','none'); ?>
                      <?php endif; ?>
</div>


<!-- end of content -->


 <?php get_sidebar(); ?>



<!-- start footer -->

<?php get_footer(); ?>

<!-- end of footer -->
