<?php

/**
* content-none.php
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
*/
?>

<div class="not-found">
	<h1><?php _e('Nothing found','mi-framework');?></h1>
</div>