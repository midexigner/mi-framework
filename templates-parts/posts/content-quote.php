<?php
/**
* content-quote.php
*
* The default template for displaying post with the quote post format.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
*/
?>

<article id="post-<?php the_ID();?>" <?php post_class(); ?>>



<!-- Article Content -->
<div class="entry-content">

	<?php


	the_content(__('Cotinue Readidng &rarr;','mi-framework'));

	wp_link_pages();



	?>

</div><!-- end entry-content -->

<!-- Article Footer -->
<footer class="entry-footer">

	<p class="entry-meta">

		<?php 

		// Displat the meta information

		mi_post_meta();

		?>

	</p>

</footer><!-- end entry-footer -->
</article>