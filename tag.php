<?php 
/**
* tag.php
*
* The template for displaying tag pages.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>

 <?php get_header(); ?>



 <div class="main-content col-md-8" role='main'>
 	
 	<?php if( have_posts()) : ?>

 		<header class="page-header">
	<h1>
	<?php 
printf( __( 'Tag Archives For &quot;%s&quot;', 'mi-framework'), single_tag_title( '', false ));
	 ?>
	</h1>
<?php 	
// Show an optional category description.
if(tag_description() ){
	echo '<p>' . tag_description() . '</p>';
}

 ?>
 </header>

<?php while( have_posts()) : the_post(); ?>
<?php get_template_part( 'content', get_post_format() ); ?>
<?php endwhile; ?>
<?php mi_paging_nav(); ?>
<?php else : ?>
	<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>
 </div><!-- end main-content -->

 <?php get_sidebar(); ?>

 <?php get_footer(); ?>