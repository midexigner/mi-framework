<?php
/**
* footer.php
*
* The template for displaying the footer.
* Package mi-framework Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright Copyright (c) 2015, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>

 </div><!-- end row-->
 </div><!-- end container-->

<!-- Footer -->
	<div class="site-footer">
		<div class="container">
<div class="row">
<?php if(is_active_sidebar('footer_column_1')):  ?>
<div class="col-md-3 col-sm-6">
<?php dynamic_sidebar('footer_column_1'); ?>
 </div>
 <?php endif; ?>
 <?php if(is_active_sidebar('footer_column_2')):  ?>
 <div class="col-md-3 col-sm-6">
 <?php dynamic_sidebar('footer_column_2'); ?>
  </div>
  <?php endif; ?>
  <?php if(is_active_sidebar('footer_column_3')):  ?>
  <div class="col-md-3 col-sm-6">
  <?php dynamic_sidebar('footer_column_3'); ?>
   </div>
   <?php endif; ?>
   <?php if(is_active_sidebar('footer_column_4')):  ?>
   <div class="col-md-3 col-sm-6">
   <?php dynamic_sidebar('footer_column_4'); ?>
    </div>
    <?php endif; ?>
</div>
		</div>
</div>
	<!-- ./Footer -->
	<!-- ./Footer nav -->
  <div class="site-footer-nav">
<div class="container">
<div class="row">
  <div class=" col-md-12">
  <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => 'footer-nav','menu_class'=> 'nav navbar-nav',  ) ); ?>
</div>
</div>
</div>
  </div>
	<!-- ./Footer nav -->
  <!-- ./ Footer Bottom -->
  <div class="site-footer-bottom">
  <div class="container">
 <div class="row">
   <?php if(is_active_sidebar('footer_bottom_left')):  ?>
  <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-left">
<?php dynamic_sidebar('footer_bottom_left'); ?>
  </div><!-- end of Footer Bottom left -->
<?php else: ?>
<div class="col-md-12 col-sm-12 col-xs-12 text-center">
<?php footer_bottom_text(); ?>
</div>
  <?php endif; ?>
  <?php if(is_active_sidebar('footer_bottom_right')):  ?>
 <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-right">
<?php dynamic_sidebar('footer_bottom_right'); ?>
 </div> <!-- end of Footer Bottom right -->
 <?php endif; ?>
 <?php if(is_active_sidebar('footer_bottom_center')):  ?>
   <div class="col-md-12 text-center">
<?php dynamic_sidebar('footer_bottom_center'); ?>
   </div> <!-- end of Footer Bottom center -->

 <?php endif; ?>

  </div>
</div>
</div>
    <!-- ./ Footer Bottom -->
  </div>
<?php wp_footer(); ?>
